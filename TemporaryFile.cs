namespace HushWeb;

/// <summary>
/// Automatically creates a temporary file and deletes it when it's no longer needed. Useful for
/// passing data to shell commands.
/// </summary>
public class TemporaryFile : IDisposable
{
    private readonly string path = null!;
    private bool isDisposed;

    /// <summary>
    /// Initializes a new instance of the <see cref="TemporaryFile"/> class.
    /// </summary>
    /// <param name="content">File content</param>
    /// <param name="temporaryFilesDirectory">
    /// Directory to create the file in. Directory will be created (recursively) if it doesn't exist.
    /// </param>
    public TemporaryFile(string content, string temporaryFilesDirectory)
    {
        var uuid = Guid.NewGuid().ToString();
        Directory.CreateDirectory(temporaryFilesDirectory);
        path = System.IO.Path.Join(temporaryFilesDirectory, uuid);
        File.WriteAllText(path, content);
    }

    /// <summary>
    /// Finalizes an instance of the <see cref="TemporaryFile"/> class.
    /// </summary>
    ~TemporaryFile()
    {
        Dispose(disposing: false);
    }

    /// <summary>
    /// Absolute path to the temporary file that was created
    /// </summary>
    public string Path => path;

    /// <inheritdoc/>
    void IDisposable.Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Disposes the <see cref="TemporaryFile"/>
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
        if (!isDisposed)
        {
            File.Delete(Path);
            isDisposed = true;
        }
    }
}
