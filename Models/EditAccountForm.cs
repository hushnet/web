namespace HushWeb.Models;

using System.ComponentModel.DataAnnotations;
using HushWeb.Validators;

/// <summary>
/// Form used for a user to edit their own account details.
/// </summary>
public class EditAccountForm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="EditAccountForm"/> class.
    /// </summary>
    public EditAccountForm() { }

    /// <summary>
    /// Unique ID of the account to edit. This is set using a hidden input.
    /// </summary>
    [Display(Name = "Unique ID")]
    [Required]
    public string UniqueId { get; set; } = null!;

    /// <summary>
    /// Token of the account to edit. This is set using a hidden input. See:
    /// <see cref="Services.AuthenticationKeystore.AuthenticationKey"/>
    /// </summary>
    [Display(Name = "Token")]
    [Required]
    public string Token { get; set; } = null!;

    // /// <summary>
    // /// User's new email address.
    // /// </summary>
    // [Display(Name = "Email")]
    // [EmailAddress(ErrorMessage = "Email address is invalid")]
    // public string Email { get; set; } = null!;

    /// <summary>
    /// User's new PGP public key.
    /// </summary>
    [Display(Name = "Public Key")]
    [PgpPublicKey]
    public string PublicKey { get; set; } = null!;
}
