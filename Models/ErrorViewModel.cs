namespace HushWeb.Models;

#pragma warning disable // This file was automatically generated.

public class ErrorViewModel
{
    public string? RequestId { get; set; }

    public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
}
