namespace HushWeb.Models;

using System.ComponentModel.DataAnnotations;
using HushWeb.Validators;
using Newtonsoft.Json;
using PgpCore;

/// <summary>
/// Form used by a non-administrator to finalize the account creation process.
/// </summary>
public class ApplicationForm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ApplicationForm"/> class.
    /// </summary>
    public ApplicationForm() { }

    /// <summary>
    /// User's email address.
    /// </summary>
    [Display(Name = "Email address")]
    [Required]
    [EmailAddress]
    public string Email { get; set; } = null!;

    /// <summary>
    /// User's unique ID.
    /// </summary>
    [Display(Name = "Unique ID")]
    [Required]
    [StringLength(
        24,
        ErrorMessage = "The Unique ID field must be between 8 and 128 characters",
        MinimumLength = 4
    )]
    [LdapUniqueId]
    public string UniqueId { get; set; } = null!;

    /// <summary>
    /// User's new password.
    /// </summary>
    [Display(Name = "New password")]
    [Required]
    [StringLength(
        128,
        ErrorMessage = "The Password field must be between 8 and 128 characters",
        MinimumLength = 8
    )]
    public string Password { get; set; } = null!;

    /// <summary>
    /// 'Confirm password' field used to prevent typos in the user's password.
    /// </summary>
    [JsonIgnore]
    [Display(Name = "Confirm password")]
    [Compare("Password", ErrorMessage = "Passwords do not match")]
    public string ConfirmPassword { get; set; } = null!;

    /// <summary>
    /// User's PGP public key.
    /// </summary>
    [Display(Name = "Public key")]
    [Required]
    [PgpPublicKey]
    public string PublicKey { get; set; } = null!;

    /// <summary>
    /// Messaged signed by the user's provided key, containing the challenge
    /// from their session.
    /// </summary>
    [Display(Name = "Signed message")]
    [Required]
    [PgpChallenge("PublicKey")]
    [JsonIgnore]
    public string SignedMessage { get; set; } = null!;

    /// <summary>
    /// Gets the applicants's public key fingerprint.
    /// </summary>
    [JsonIgnore]
    public string? PublicKeyFingerprint =>
        Convert.ToHexString(
            new EncryptionKeys(PublicKey).PublicKey.GetFingerprint()
        );
}
