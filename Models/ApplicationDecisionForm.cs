namespace HushWeb.Models;

using System.ComponentModel.DataAnnotations;

/// <summary>
/// Used by an admin to approve or reject an application.
/// </summary>
public class ApplicationDecisionForm : ArbitraryPermissionBlockForm
{
    /// <summary>
    /// Should be "Approve" or "Reject".
    /// </summary>
    [Required]
    public string Action { get; set; } = null!;
}
