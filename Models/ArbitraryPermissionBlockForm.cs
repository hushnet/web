namespace HushWeb.Models;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using HushWeb.Validators;

/// <summary>
/// Form used by an administrator to validate an arbitrary permission block.
/// </summary>
public class ArbitraryPermissionBlockForm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ArbitraryPermissionBlockForm"/> class.
    /// </summary>
    public ArbitraryPermissionBlockForm() { }

    /// <summary>
    /// Any type of permission block.
    /// </summary>
    [Display(Name = "Permission Block")]
    [Required]
    [PgpSignedByAdmin]
    [ContainsPermissionBlock]
    public string PermissionBlock { get; set; } = null!;
}
