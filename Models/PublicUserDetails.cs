namespace HushWeb.Models;

using Newtonsoft.Json;

/// <summary>
/// User details which should be public to everyone, including unregistered
/// guests.
/// </summary>
public struct PublicUserDetails
{
    /// <summary>
    /// Unique ID of the user.
    /// </summary>
    [JsonProperty("uid")]
    public List<string> UniqueId { get; set; }

    /// <summary>
    /// Email address of the user.
    /// </summary>
    [JsonProperty("mail")]
    public List<string> Email { get; set; }
}
