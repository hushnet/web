namespace HushWeb.Models;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using HushWeb.Validators;

/// <summary>
/// Form used by an administrator to perform an arbitrary ldapsearch.
/// </summary>
public class ArbitraryLdapSearchForm
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ArbitraryLdapSearchForm"/> class.
    /// </summary>
    public ArbitraryLdapSearchForm() { }

    /// <summary>
    /// LDAP bind DN.
    /// </summary>
    [Display(Name = "Bind DN")]
    [Required]
    public string BindDN { get; set; } = null!;

    /// <summary>
    /// Search base for ldapsearch.
    /// </summary>
    [Display(Name = "Search Base")]
    [Required]
    public string SearchBase { get; set; } = null!;

    /// <summary>
    /// Search scope for ldapsearch.
    /// </summary>
    [Display(Name = "Scope")]
    [Required]
    public string Scope { get; set; } = null!;

    /// <summary>
    /// Search filter for ldapsearch.
    /// </summary>
    [Display(Name = "Filter")]
    public string Filter { get; set; } = null!;

    /// <summary>
    /// LDAP bind password.
    /// </summary>
    [Display(Name = "Password")]
    [Required]
    public string Password { get; set; } = null!;
}
