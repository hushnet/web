namespace HushWeb;

/// <summary>
/// Static functions used to generate route data to display a notice
/// </summary>
public static class Notice
{
    /// <summary>
    /// Generates route data to display an info notice
    /// </summary>
    /// <param name="text">Info text</param>
    /// <returns>Object to be used as route data</returns>
    public static dynamic Info(string text) =>
        new { noticeType = "info", noticeText = text };

    /// <summary>
    /// Generates route data to display an error notice
    /// </summary>
    /// <param name="text">Error text</param>
    /// <returns>Object to be used as route data</returns>
    public static dynamic Error(string text) =>
        new { noticeType = "error", noticeText = text };
}
