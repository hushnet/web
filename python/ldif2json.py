from ldif import LDIFParser
from sys import argv, stdout
from json import dumps

parser = LDIFParser(open(argv[1], "rb"))
output = dict()
for dn, record in parser.parse():
    record["dn"] = dn
    output[dn] = record
stdout.write(dumps(output))
