namespace HushWeb;

using Microsoft.AspNetCore.Mvc.ModelBinding;

/// <summary>
/// Useful ModelState extensions
/// </summary>
public static class ModelStateUtil
{
    private const char Bullet = '\u2022';

    /// <summary>
    /// Parses ModelState to extract errors into a readable format
    /// </summary>
    /// <param name="modelState">
    /// ModelState to extract validation errors from
    /// </param>
    /// <returns>
    /// Bullet-point list of validation errors. Output starts with a newline.
    /// </returns>
    public static string ToBulletList(this ModelStateDictionary modelState)
    {
        var output = string.Empty;
        var errors = modelState.Values.SelectMany(m => m.Errors);
        foreach (var error in errors)
        {
            output += $"\n{Bullet} {error.ErrorMessage}"
                .Replace("The ", string.Empty)
                .Replace(" field", string.Empty);
        }

        return output;
    }
}
