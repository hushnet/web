﻿namespace HushWeb.Controllers;

using Microsoft.AspNetCore.Mvc;
using HushWeb.Models;
using HushWeb.Services.LdapUtils;
using HushWeb.Services.CryptoPermissions;
using Newtonsoft.Json;
using HushWeb.Services.ApplicationStore;
using HushWeb.Services.PgpKeystore;

/// <summary>
/// Controller for administrative actions. While it is publically accessible,
/// performing any real changes requires administrative permission.
/// </summary>
public class AdminController : Controller
{
    private readonly LdapUtilsService ldap;
    private readonly CryptoPermissionsService cryptoPermissions;
    private readonly ApplicationStoreService applicationStore;
    private readonly PgpKeystoreService pgpKeystore;

    /// <summary>
    /// Initializes a new instance of the <see cref="AdminController"/> class.
    /// </summary>
    /// <param name="ldap">
    /// Service used to interact with LDAP server.
    /// </param>
    /// <param name="cryptoPermissions">
    /// Service used to interact with permission blocks.
    /// </param>
    /// <param name="applicationStore">
    /// Service used to interact with application store.
    /// </param>
    /// <param name="pgpKeystore">
    /// Service used to interact with PGP public keystore.
    /// </param>
    public AdminController(
        LdapUtilsService ldap,
        CryptoPermissionsService cryptoPermissions,
        ApplicationStoreService applicationStore,
        PgpKeystoreService pgpKeystore
    )
    {
        this.cryptoPermissions = cryptoPermissions;
        this.ldap = ldap;
        this.applicationStore = applicationStore;
        this.pgpKeystore = pgpKeystore;
    }

    /// <summary>
    /// Admin homepage. Contains a list of links to administrative tools.
    /// </summary>
    public IActionResult Index()
    {
        return View();
    }

    /// <summary>
    /// View containg a form to submit a permission block to be decrypted and
    /// decoded.
    /// </summary>
    public IActionResult DecodePermissionBlock() => View();

    /// <summary>
    /// Handles submission of permission block messages to be decoded.
    /// </summary>
    /// <param name="message">
    /// Permission block message to be decoded.
    /// </param>
    /// <returns>
    /// JSON encoding of permission block content.
    /// </returns>
    public IActionResult SubmitDecodePermissionBlock([FromForm] string message)
    {
        var json = cryptoPermissions.DecryptPermissionBlock(message);
        var obj = JsonConvert.DeserializeObject(json);
        var indented = JsonConvert.SerializeObject(obj, Formatting.Indented);
        return Content(indented);
    }

    /// <summary>
    /// Looks up a view containing a form to create a permission block of the
    /// specified type.
    /// </summary>
    /// <param name="id">
    /// Name of the view to lookup.
    /// </param>
    /// <returns>
    /// View containing a permission block form.
    /// </returns>
    public IActionResult EncodePermissionBlock([FromRoute] string id)
    {
        if (!PermissionType.AllTypes.Contains(id))
        {
            return Content("Permission type was not found.");
        }

        return View($"Views/Admin/EncodePermissionBlock/{id}.cshtml");
    }

    /// <summary>
    /// Contains <see cref="ArbitraryLdapSearchForm"/>.
    /// </summary>
    public IActionResult ArbitraryLdapSearch() => View();

    /// <summary>
    /// Handles submission of the <see cref="ArbitraryLdapSearchForm"/>.
    /// Executes an arbitrary ldapsearch.
    /// </summary>
    /// <param name="model">
    /// Form to validate and process.
    /// </param>
    /// <returns>
    /// LDIF results from the ldapsearch.
    /// </returns>
    public async Task<IActionResult> SubmitArbitraryLdapSearch(
        [FromForm] ArbitraryLdapSearchForm model
    )
    {
        if (!ModelState.IsValid)
        {
            return Content($"Validation errors:{ModelState.ToBulletList()}");
        }

        // TODO: Get these strings from the Admin static class
        if (
            model.BindDN != "cn=admin,dc=hush,dc=rip"
            && model.BindDN != "cn=config,dc=hush,dc=rip"
        )
        {
            return Content("Invalid Bind DN");
        }

        var result = await ldap.SearchRaw(
            model.BindDN,
            model.SearchBase,
            model.Password,
            model.Scope,
            model.Filter
        );
        return Content(
            result.ExitCode == 0 ? "LDAP search failed." : result.Output
        );
    }

    /// <summary>
    /// Used to submit a <see cref="DeleteAccountPermissionBlock"/>.
    /// </summary>
    public IActionResult DeleteAccount() => View();

    /// <summary>
    /// Validates the permission block and deletes a user account.
    /// </summary>
    public async Task<IActionResult> SubmitDeleteAccount(
        [FromForm] ArbitraryPermissionBlockForm model
    )
    {
        if (!ModelState.IsValid)
        {
            var notice =
                "Account deletion failed due to the following validation errors:";
            notice += ModelState.ToBulletList();
            return Content(notice);
        }

        DeleteAccountPermissionBlock? permissionBlock = null;

        try
        {
            var json = cryptoPermissions.DecryptPermissionBlock(
                model.PermissionBlock
            );
            permissionBlock =
                JsonConvert.DeserializeObject<DeleteAccountPermissionBlock>(
                    json
                );
        }
        catch { }

        if (permissionBlock == null)
        {
            return Content("Failed to decode permission block.");
        }

        if (!permissionBlock.IsValid())
        {
            return Content("error", "Invalid permission block.");
        }

        var deleted = await LdapHelper.DeleteUser(
            ldap,
            permissionBlock.UniqueId!
        );

        if (!deleted)
        {
            return Content(
                $"Failed to delete user account: {permissionBlock.UniqueId}"
            );
        }

        return Content($"Deleted user account: {permissionBlock.UniqueId}");
    }

    /// <summary>
    /// Allows admin to view applications.
    /// </summary>
    /// <param name="noticeType">
    /// Notice type for form result.
    /// </param>
    /// <param name="noticeText">
    /// Notice text for form result.
    /// </param>
    /// <param name="uniqueId">
    /// Applicant unique ID.
    /// </param>
    public async Task<IActionResult> ViewApplication(
        string? noticeType,
        string? noticeText,
        [FromForm] string? uniqueId
    )
    {
        if (noticeType != null && noticeText != null)
        {
            ViewBag.NoticeType = noticeType;
            ViewBag.NoticeText = noticeText;
        }

        if (!string.IsNullOrWhiteSpace(uniqueId))
        {
            ViewBag.uniqueId = uniqueId;
            ViewBag.permissionBlock = await applicationStore.GetApplication(
                uniqueId
            );
        }

        ViewBag.applicantUniqueIds =
            await applicationStore.GetAllApplicantUniqueIds();

        return View();
    }

    /// <summary>
    /// Approves or rejects an application.
    /// </summary>
    /// <param name="model">
    /// <see cref="ApplicationDecisionForm"/> containing a signed permission
    /// block and action type.
    /// </param>
    public async Task<IActionResult> SubmitApplication(
        [FromForm] ApplicationDecisionForm model
    )
    {
        // Validate and decrypt
        if (!ModelState.IsValid)
        {
            var notice =
                "Application action failed due to the following validation errors:";
            notice += ModelState.ToBulletList();

            return RedirectToAction("ViewApplication", Notice.Error(notice));
        }

        var decrypted = cryptoPermissions.DecryptPermissionBlock(
            model.PermissionBlock
        );
        var permissionBlock =
            JsonConvert.DeserializeObject<CreateAccountPermissionBlock>(
                decrypted
            );

        if (permissionBlock == null || !permissionBlock.IsValid())
        {
            return RedirectToAction(
                "ViewApplication",
                Notice.Error("Invalid permission block.")
            );
        }

        var uniqueId = permissionBlock.Application.UniqueId;

        // Complete action
        switch (model.Action)
        {
            case "Approve":
                try
                {
                    await AcceptApplication(permissionBlock.Application);
                }
                catch (Exception e)
                {
                    return RedirectToAction(
                        "ViewApplication",
                        Notice.Error($"User creation failed: {e.Message}")
                    );
                }

                applicationStore.DeleteApplication(uniqueId);

                return RedirectToAction(
                    "ViewApplication",
                    Notice.Info($"Application approved: {uniqueId}")
                );
            case "Reject":
                applicationStore.DeleteApplication(uniqueId);
                return RedirectToAction(
                    "ViewApplication",
                    Notice.Info($"Application rejected: {uniqueId}")
                );
            default:
                return RedirectToAction(
                    "ViewApplication",
                    Notice.Error($"Invalid application action type.")
                );
        }
    }

#region EncodePermissionBlock

    /// <summary>
    /// Handles submission of the <see cref="CreateAccountPermissionBlock"/>.
    /// </summary>
    /// <param name="model">
    /// Form to validate and process.
    /// </param>
    /// <returns>
    /// Rendered <see cref="CreateAccountPermissionBlock"/>.
    /// </returns>
    public IActionResult EncodePermissionBlock_CreateAccount(
        [FromForm] CreateAccountPermissionBlock model
    ) => Content(cryptoPermissions.RenderBlock(model));

    /// <summary>
    /// Handles submission of the <see cref="DeleteAccountPermissionBlock"/>.
    /// </summary>
    /// <param name="model">
    /// Form to validate and process.
    /// </param>
    /// <returns>
    /// Rendered <see cref="DeleteAccountPermissionBlock"/>.
    /// </returns>
    public IActionResult EncodePermissionBlock_DeleteAccount(
        [FromForm] DeleteAccountPermissionBlock model
    ) => Content(cryptoPermissions.RenderBlock(model));

#endregion

    private async Task AcceptApplication(ApplicationForm model)
    {
        var created = await LdapHelper.CreateUser(
            ldap,
            "users",
            model.UniqueId,
            model.Email,
            model.Password
        );

        if (!created)
        {
            throw new Exception("Failed to create LDAP user.");
        }

        var pgpStored = await pgpKeystore.StoreKey(
            model.UniqueId,
            model.PublicKey
        );

        if (!pgpStored)
        {
            var deleted = await LdapHelper.DeleteUser(ldap, model.UniqueId);
            if (deleted)
            {
                throw new Exception(
                    "CRITICAL FAILURE: Failed to delete LDAP user after PGP public key storage failed. Manual intervention needed!"
                );
            }

            throw new Exception(
                "Failed to store user's PGP public key. LDAP user deleted."
            );
        }
    }
}
