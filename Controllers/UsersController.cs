﻿namespace HushWeb.Controllers;

using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using HushWeb.Models;
using HushWeb.Services.LdapUtils;
using HushWeb.Services.ApplicationStore;
using HushWeb.Services.PgpKeystore;

/// <summary>
/// Controller for actions relating to public user details.
/// </summary>
public class UsersController : Controller
{
    private readonly LdapUtilsService ldap;
    private readonly ApplicationStoreService applicationStore;
    private readonly PgpKeystoreService pgpKeystore;

    /// <summary>
    /// Initializes a new instance of the <see cref="UsersController"/> class.
    /// </summary>
    /// <param name="ldap">
    /// Service used to interact with LDAP server.
    /// </param>
    /// <param name="applicationStore">
    /// Service used to manage application storage.
    /// </param>
    /// <param name="pgpKeystore">
    /// Service used to fetch users' PGP public keys.
    /// </param>
    public UsersController(
        LdapUtilsService ldap,
        ApplicationStoreService applicationStore,
        PgpKeystoreService pgpKeystore
    )
    {
        this.ldap = ldap;
        this.applicationStore = applicationStore;
        this.pgpKeystore = pgpKeystore;
    }

    /// <summary>
    /// Executes an ldapsearch of all user accounts, then checks applications.
    /// </summary>
    /// <returns>
    /// User list view.
    /// </returns>
    public async Task<IActionResult> Index()
    {
        var users = await ldap.SearchAnonymous<PublicUserDetails>(
            searchBase: "ou=users,dc=hush,dc=rip",
            filter: "(objectClass=inetOrgPerson)"
        );

        if (users == null)
        {
            throw new Exception("Search failed");
        }

        ViewBag.users = users.Values.ToList();
        ViewBag.pendingUsers =
            await applicationStore.GetAllApplicantUniqueIds();

        return View();
    }

    /// <summary>
    /// Serves a user's PGP public key file.
    /// </summary>
    /// <param name="id">
    /// Unique ID of the user.
    /// </param>
    /// <returns>
    /// PGP public key file download.
    /// </returns>
    public IActionResult PublicKey(string id)
    {
        if (!pgpKeystore.UserHasKey(id))
        {
            return NotFound();
        }

        var filePath = pgpKeystore.KeyPath(id);
        var fileName = Path.GetFileName(filePath);
        var stream = System.IO.File.Open(
            filePath,
            FileMode.Open,
            FileAccess.Read
        );

        return File(stream, "application/pgp-signature", fileName);
    }
}
