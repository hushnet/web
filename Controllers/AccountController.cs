﻿namespace HushWeb.Controllers;

using Microsoft.AspNetCore.Mvc;
using HushWeb.Models;
using HushWeb.Services.LdapUtils;
using HushWeb.Services.AuthenticationKeystore;
using HushWeb.Services.PgpKeystore;

/// <summary>
/// Controller for actions that enable a non-administrator user to modify their
/// own account details. This includes both public and private account details.
/// </summary>
public class AccountController : Controller
{
    private const string TokenCookie = "Token.Account";

    private readonly LdapUtilsService ldap;
    private readonly AuthenticationKeystoreService authKeystore;
    private readonly PgpKeystoreService pgpKeystore;

    /// <summary>
    /// Initializes a new instance of the <see cref="AccountController"/> class.
    /// </summary>
    /// <param name="ldap">
    /// Service used to interact with LDAP server.
    /// </param>
    /// <param name="authKeystore">
    /// Service used to interact with authentication keystore.
    /// </param>
    /// <param name="pgpKeystore">
    /// Service used to interact with PGP public keystore.
    /// </param>
    public AccountController(
        LdapUtilsService ldap,
        AuthenticationKeystoreService authKeystore,
        PgpKeystoreService pgpKeystore
    )
    {
        this.ldap = ldap;
        this.authKeystore = authKeystore;
        this.pgpKeystore = pgpKeystore;
    }

    /// <summary>
    /// AccountController index page. Contains an authentication form.
    /// </summary>
    /// <param name="noticeType">
    /// Notice type for form result.
    /// </param>
    /// <param name="noticeText">
    /// Notice text for form result.
    /// </param>
    /// <returns>
    /// View containing a form for user authentication, and a notice with the
    /// form result. Notice can be used for either the authentication form or
    /// account details edit form.
    /// </returns>
    public IActionResult Index(string? noticeType, string? noticeText)
    {
        // TODO: Enable this again
        // if (noticeType != null && noticeText != null)
        // {
        //     ViewBag.NoticeType = noticeType;
        //     ViewBag.NoticeText = noticeText;
        // }

        // return View();
        return RedirectToAction("Index", "Home");
    }

    /// <summary>
    /// Handles submission of user credentials for authentication.
    /// </summary>
    /// <param name="uniqueId">
    /// User's unique ID.
    /// </param>
    /// <param name="password">
    /// User's password.
    /// </param>
    /// <returns>
    /// If credentials are valid, redirects to the <see cref="Edit"/> action.
    /// Otherwise, redirects back to index and displays a notice to inform the
    /// user that the specified credentials are invalid.
    /// </returns>
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Auth(
        [FromForm] string uniqueId,
        [FromForm] string password
    )
    {
        bool validAuth = await LdapHelper.AuthenticateUser(
            this.ldap,
            uniqueId,
            password
        );

        if (!validAuth)
        {
            return RedirectToAction(
                "Index",
                Notice.Error(
                    "Authentication failed: Your credentials are incorrect or the user does not exist."
                )
            );
        }

        var key = new AuthenticationKey(uniqueId, password);
        authKeystore.RemoveExpired();
        authKeystore.RemoveWithUniqueId(uniqueId);
        authKeystore.StoreKey(key);

        Response.Cookies.Append(
            TokenCookie,
            key.Token,
            new() { Expires = DateTime.Now.Add(AuthenticationKey.Duration) }
        );

        return RedirectToAction("Edit", new { uniqueId });
    }

    /// <summary>
    /// Enables the user to edit their own account details.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the user to edit.
    /// </param>
    /// <returns>
    /// If no valid authentication token is found in the user's cookies,
    /// redirects back to <see cref="Index"/>. Otherwise, displays a form to
    /// edit account details.
    /// </returns>
    public async Task<IActionResult> Edit(string uniqueId)
    {
        var token = Request.Cookies[TokenCookie];

        if (token == null)
        {
            return RedirectToAction(
                "Index",
                Notice.Error(
                    $"Authentication failed: No token was present in your request."
                )
            );
        }

        var key = authKeystore.GetKey(uniqueId, token);

        if (key == null)
        {
            return RedirectToAction(
                "Index",
                Notice.Error(
                    $"Authentication failed: Your token is expired, revoked, or invalid."
                )
            );
        }

        // Get public details from LDAP
        var sanitizedUid = LdapHelper.EncodeDN(uniqueId);
        var users = await ldap.SearchAnonymous<PublicUserDetails>(
            searchBase: "ou=users,dc=hush,dc=rip",
            filter: $"(uid={sanitizedUid})"
        );
        var userFullDN = $"cn={sanitizedUid},ou=users,dc=hush,dc=rip";

        if (users == null || users.Count != 1 || !users.ContainsKey(userFullDN))
        {
            return RedirectToAction(
                "Index",
                Notice.Error($"Failed to find account for user: {uniqueId}")
            );
        }

        var user = users[userFullDN];

        // Get PGP public key from keystore
        string? publicKey = null;

        if (pgpKeystore.UserHasKey(uniqueId))
        {
            publicKey = await pgpKeystore.GetKey(uniqueId);
        }

        var model = new EditAccountForm()
        {
            UniqueId = uniqueId,
            Token = token,
            // Email = user.Email ?? string.Empty,
            PublicKey = publicKey ?? string.Empty,
        };

        return View(model);
    }

    /// <summary>
    /// Validates and saves the edits the user wants to make to their account.
    /// </summary>
    /// <param name="model">
    /// Form containing the account detail edits.
    /// </param>
    /// <returns>
    /// Redirect to <see cref="Index"/> with a notice about the result of the
    /// operation.
    /// </returns>
    public async Task<IActionResult> Save([FromForm] EditAccountForm model)
    {
        var token = Request.Cookies[TokenCookie];

        if (token == null)
        {
            return RedirectToAction(
                "Index",
                Notice.Error(
                    $"Authentication failed: Your token is expired, revoked, or invalid."
                )
            );
        }

        if (!ModelState.IsValid)
        {
            var notice =
                "Account update failed due to the following validation errors:";
            notice += ModelState.ToBulletList();

            return RedirectToAction("Index", Notice.Error(notice));
        }

        // Store LDAP account changes
        // var changes = new List<LdapChange>()
        // {
        //     new()
        //     {
        //         ChangeType = LdapChangeType.Replace,
        //         Attribute = "mail",
        //         Value = model.Email,
        //         ConvertToBase64 = false,
        //     },
        // };

        // var sanitizedUid = LdapHelper.EncodeDN(model.UniqueId);
        // var dn = $"cn={sanitizedUid},ou=readers,dc=hush,dc=rip";
        // var ldif = LdapHelper.GenerateModifyLdif(dn, changes);
        // await ldap.Modify(Admin.LdapDN, Admin.LdapPassword, ldif);

        // Store PGP public key changes
        if (
            pgpKeystore.UserHasKey(model.UniqueId)
            && !string.IsNullOrWhiteSpace(model.PublicKey)
        )
        {
            pgpKeystore.DeleteKey(model.UniqueId);
        }

        await pgpKeystore.StoreKey(model.UniqueId, model.PublicKey.Trim());

        return RedirectToAction(
            "Index",
            Notice.Info($"Account details were successfully updated.")
        );
    }

    /// <summary>
    /// Revokes all authentication keys for the specified user.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the user.
    /// </param>
    /// <returns>
    /// Redirect to <see cref="Index"/> with a notice about the result of the
    /// operation.
    /// </returns>
    public IActionResult Revoke(string uniqueId)
    {
        var token = Request.Cookies[TokenCookie];

        if (token == null)
        {
            return RedirectToAction(
                "Index",
                Notice.Error(
                    $"Revokation failed. No token was present in your request."
                )
            );
        }

        authKeystore.RemoveWithUniqueId(uniqueId);

        return RedirectToAction(
            "Index",
            Notice.Info($"Authentication token revoked: {token}")
        );
    }
}
