namespace HushWeb.Controllers;

using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using Markdig;
using System.Text;

/// <summary>
/// Controller for document browser.
/// </summary>
public class DocsController : Controller
{
    private readonly string rootPath;

    /// <summary>
    /// Initializes a new instance of the <see cref="DocsController"/> class.
    /// </summary>
    public DocsController(IWebHostEnvironment environment)
    {
        rootPath = Path.Join(environment.WebRootPath, "docs");
    }

    /// <summary>
    /// Finds and displays directories and a document (if applicable).
    /// </summary>
    [HttpGet("Docs/{*relativePath}")]
    public async Task<IActionResult> Index(
        string relativePath,
        [FromQuery] bool raw
    )
    {
        var absolutePath = Path.GetFullPath(Path.Join(rootPath, relativePath));

        // Prevent traversing outside of rootPath
        if (!absolutePath.StartsWith(rootPath))
        {
            relativePath = string.Empty;
        }

        // Load selected file or redirect to README.md if applicable
        if (!IsDirectory(absolutePath))
        {
            if (raw)
            {
                return await GetFileContentRaw(absolutePath);
            }

            var fileName = Path.GetFileName(absolutePath);
            relativePath = relativePath[..^fileName.Length];
            ViewBag.fileName = fileName;
            ViewBag.fileContent = await GetFileContent(absolutePath);
            ViewBag.fileContentType = GetFileType(absolutePath);
        }
        else if (System.IO.File.Exists(Path.Join(absolutePath, "README.md")))
        {
            var currentUrl = Request.Path.ToString().TrimEnd('/');
            return Redirect($"{currentUrl}/README.md");
        }

        ViewBag.pathParts = FormatCurrentPath(relativePath);
        ViewBag.entries = GetEntries(relativePath);

        return View();
    }

    private static List<(string display, string href)> FormatCurrentPath(
        string path
    )
    {
        var partTuples = new List<(string display, string href)>()
        {
            ("Docs", "/Docs"),
        };

        if (string.IsNullOrWhiteSpace(path))
        {
            return partTuples;
        }

        var parts = path.Split('/').Where(p => !string.IsNullOrWhiteSpace(p));

        foreach (var part in parts)
        {
            partTuples.Add((part, $"{partTuples.Last().href}/{part}"));
        }

        return partTuples;
    }

    private static bool IsDirectory(string path)
    {
        var attributes = System.IO.File.GetAttributes(path);
        return (attributes & FileAttributes.Directory)
            == FileAttributes.Directory;
    }

    private static string GetFileType(string filePath)
    {
        if (IsDirectory(filePath))
        {
            return "Directory";
        }

        return Path.GetExtension(filePath) switch
        {
            ".png" or ".jpg" => "Image",
            ".txt" => "Text",
            ".md" => "Markdown",
            _ => "Unknown",
        };
    }

    private async Task<string> GetFileContent(string absolutePath)
    {
        var type = GetFileType(absolutePath);
        var content = await System.IO.File.ReadAllTextAsync(absolutePath);

        content = type switch
        {
            "Text" => $"<pre style=\"background: none\">{content}</pre>",
            "Markdown" => Markdown.ToHtml(content),
            "Image"
                => $"<img src=\"{HttpContext.Request.Path}?raw=true\" style=\"border: 1px solid #a1a1a1; max-width: 670px;\">",
            _ => $"<pre>{content}</pre>",
        };

        return content;
    }

    private async Task<IActionResult> GetFileContentRaw(string absolutePath)
    {
        if (IsDirectory(absolutePath))
        {
            return Content("Requested resource is a directory.");
        }

        var provider = new FileExtensionContentTypeProvider();
        provider.TryGetContentType(
            Path.GetFileName(absolutePath),
            out string? contentType
        );
        contentType ??= "text/plain";
        string superType = contentType.Split('/')[0];

        switch (superType)
        {
            case "audio":
            case "video":
            case "image":
                var stream = System.IO.File.Open(
                    absolutePath,
                    FileMode.Open,
                    FileAccess.Read
                );
                return File(stream, contentType);
            case "text":
                var content = await System.IO.File.ReadAllTextAsync(
                    absolutePath
                );
                return Content(content, contentType, Encoding.UTF8);
        }

        return Content(
            $@"Requested resource could not be loaded.
Unsupported mime type: {contentType}"
        );
    }

    private (string name, string type) CreateEntryTuple(string path)
    {
        return (path[rootPath.Length..], GetFileType(path));
    }

    private List<(string name, string type)> GetEntries(string relativePath)
    {
        var absolutePath = Path.GetFullPath(Path.Join(rootPath, relativePath));

        if (!absolutePath.StartsWith(rootPath))
        {
            return new();
        }

        var entries = Directory.GetFileSystemEntries(absolutePath).ToList();

        if (!string.IsNullOrWhiteSpace(relativePath))
        {
            entries.Insert(0, Path.Join(absolutePath, ".."));
        }

        return entries
            .Select(path => CreateEntryTuple(path))
            .OrderBy(e => e.type != "Directory")
            .ThenBy(e => e.name)
            .ToList();
    }
}
