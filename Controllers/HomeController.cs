﻿namespace HushWeb.Controllers;

using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using HushWeb.Models;

/// <summary>
/// Controller for homepage.
/// </summary>
public class HomeController : Controller
{
    /// <summary>
    /// Homepage.
    /// </summary>
    public IActionResult Index()
    {
        return View();
    }

#pragma warning disable // This method was automatically generated.
    [ResponseCache(
        Duration = 0,
        Location = ResponseCacheLocation.None,
        NoStore = true
    )]
    public IActionResult Error()
    {
        return View(
            new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
            }
        );
    }
}
