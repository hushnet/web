namespace HushWeb.Controllers;

using Microsoft.AspNetCore.Mvc;
using HushWeb.Models;
using HushWeb.Services.LdapUtils;
using HushWeb.Services.CryptoPermissions;
using HushWeb.Services.ApplicationStore;

/// <summary>
/// Controller for providing application information and finalizing the account
/// creation process.
/// </summary>
public class ApplyController : Controller
{
    /// <summary>
    /// Key used to get the challenge string with
    /// HttpContext.Session.GetString()
    /// </summary>
    public const string ChallengeSessionKey = "Apply.Challenge";

    private readonly LdapUtilsService ldap;
    private readonly CryptoPermissionsService cryptoPermissions;
    private readonly ApplicationStoreService applicationStore;

    /// <summary>
    /// Initializes a new instance of the <see cref="ApplyController"/> class.
    /// </summary>
    /// <param name="ldap">
    /// Service used to interact with LDAP server.
    /// </param>
    /// <param name="cryptoPermissions">
    /// Service used to interact with permission blocks.
    /// </param>
    /// <param name="applicationStore">
    /// Service used to manage application storage.
    /// </param>
    public ApplyController(
        LdapUtilsService ldap,
        CryptoPermissionsService cryptoPermissions,
        ApplicationStoreService applicationStore
    )
    {
        this.ldap = ldap;
        this.cryptoPermissions = cryptoPermissions;
        this.applicationStore = applicationStore;
    }

    /// <summary>
    /// ApplyController index page.
    /// </summary>
    /// <param name="noticeType">
    /// Notice type for form result.
    /// </param>
    /// <param name="noticeText">
    /// Notice text for form result.
    /// </param>
    /// <returns>
    /// View containing a form for finalizing the account creation process, and
    /// a notice with the form result.
    /// </returns>
    public IActionResult Index(string? noticeType, string? noticeText)
    {
        if (noticeType != null && noticeText != null)
        {
            ViewBag.NoticeType = noticeType;
            ViewBag.NoticeText = noticeText;
        }

        var challenge = Guid.NewGuid().ToString().ToChallenge();
        ViewBag.Challenge = challenge;
        HttpContext.Session.SetString(ChallengeSessionKey, challenge);

        return View();
    }

    /// <summary>
    /// Handles submission of the <see cref="ApplicationForm"/>.
    /// </summary>
    /// <param name="model">
    /// Form to validate and process.
    /// </param>
    /// <returns>
    /// Redirects back to <see cref="Index"/> with a notice about the
    /// operation's result.
    /// </returns>
    public async Task<IActionResult> Submit([FromForm] ApplicationForm model)
    {
        dynamic Notice(string type, string notice) =>
            Url.RouteUrl(
                new
                {
                    controller = "Apply",
                    action = "Index",
                    noticeType = type,
                    noticeText = notice,
                }
            ) + "#application-form";

        if (!ModelState.IsValid)
        {
            var notice =
                "Application failed due to the following validation errors:";
            notice += ModelState.ToBulletList();
            return Redirect(Notice("error", notice));
        }

        var (success, reason) = await applicationStore.StoreApplication(model);

        if (!success)
        {
            return Redirect(
                Notice("error", $"Application submission failed: {reason}")
            );
        }

        return Redirect(Notice("info", "Application submitted."));
    }
}
