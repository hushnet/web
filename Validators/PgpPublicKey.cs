namespace HushWeb.Validators;

using System.ComponentModel.DataAnnotations;
using System.Text;
using PgpCore;

/// <summary>
/// Validator used to check if input is a valid PGP public key.
/// </summary>
public class PgpPublicKey : ValidationAttribute
{
    private const string BeginBlock = "-----BEGIN PGP PUBLIC KEY BLOCK-----";
    private const string EndBlock = "-----END PGP PUBLIC KEY BLOCK-----";

    /// <summary>
    /// Checks if input is a valid PGP public key.
    /// </summary>
    protected override ValidationResult? IsValid(
        object? value,
        ValidationContext context
    )
    {
        var key = (string?)value;

        if (key == null)
        {
            return ValidationResult.Success;
        }

        key = key.Trim();

        if (!key.StartsWith(BeginBlock) || !key.EndsWith(EndBlock))
        {
            return new ValidationResult(
                $"The {context.DisplayName} field is not a PGP public key block"
            );
        }

        try
        {
            using var pgp = new PGP(new EncryptionKeys(key));
            pgp.EncryptArmoredString("TEST");
        }
        catch (Exception e)
        {
            return new ValidationResult(
                $"The {context.DisplayName} field is invalid: {e.Message}"
            );
        }

        return ValidationResult.Success;
    }
}
