namespace HushWeb.Validators;

using System.ComponentModel.DataAnnotations;
using System.Text;
using HushWeb.Controllers;
using PgpCore;

/// <summary>
/// Validator used to check if input is a valid PGP message which was signed by
/// the user's provided PGP key and contains the correct challenge string.
/// </summary>
public class PgpChallenge : ValidationAttribute
{
    private const string BeginSignedMessage =
        "-----BEGIN PGP SIGNED MESSAGE-----";
    private const string BeginSignature = "-----BEGIN PGP SIGNATURE-----";
    private const string EndSignature = "-----END PGP SIGNATURE-----";

    private readonly string publicKeyPropertyName;

    /// <summary>
    /// Initializes a new instance of the <see cref="PgpChallenge"/> class.
    /// </summary>
    /// <param name="publicKeyPropertyName">
    /// Name of the model property that contains the user's PGP public key.
    /// </param>
    public PgpChallenge(string publicKeyPropertyName)
    {
        this.publicKeyPropertyName = publicKeyPropertyName;
    }

    /// <summary>
    /// Obtains the challenge string from session
    /// </summary>
    /// <param name="context">
    /// Validation context use to obtain HttpContext
    /// </param>
    public string GetChallenge(ValidationContext context)
    {
        var httpContextAccessor = (IHttpContextAccessor?)
            context.GetService(typeof(IHttpContextAccessor));

        if (httpContextAccessor == null)
        {
            throw new Exception("Failed to get IHttpContextAccessor service");
        }

        if (httpContextAccessor.HttpContext == null)
        {
            throw new Exception("Failed to get HttpContext");
        }

        var httpContext = httpContextAccessor.HttpContext;
        var challenge = httpContext.Session.GetString(
            ApplyController.ChallengeSessionKey
        );

        if (string.IsNullOrWhiteSpace(challenge))
        {
            throw new Exception("Failed to get challenge string from session");
        }

        return challenge;
    }

    /// <summary>
    /// Checks if input is a valid PGP message which was signed by the user's
    /// provided PGP key and contains the correct challenge string.
    /// </summary>
    protected override ValidationResult? IsValid(
        object? value,
        ValidationContext context
    )
    {
        // Get the challenge
        string challenge;

        try
        {
            challenge = GetChallenge(context);
        }
        catch (Exception e)
        {
            return new ValidationResult(
                $"The {context.DisplayName} field threw exception: {e.Message}."
            );

            throw;
        }

        // Check if input is null
        var message = (string?)value;

        if (string.IsNullOrWhiteSpace(message))
        {
            return ValidationResult.Success;
        }

        // Check if input is a valid signed message (clear)
        message = message.Trim();

        if (
            !message.StartsWith(BeginSignedMessage)
            || !message.Contains(BeginSignature)
            || !message.EndsWith(EndSignature)
        )
        {
            return new ValidationResult(
                $"The {context.DisplayName} field is not a clear, PGP signed message."
            );
        }

        // Check if message is signed by the provided public key
        if (!SignedByUser(context, message))
        {
            return new ValidationResult(
                $"The {context.DisplayName} field is not signed by the correct key."
            );
        }

        // Check if message contains the challenge string
        if (!message.Contains(challenge))
        {
            return new ValidationResult(
                $"The {context.DisplayName} field does not contain the challenge string."
            );
        }

        return ValidationResult.Success;
    }

    private bool SignedByUser(ValidationContext context, string message)
    {
        try
        {
            var publicKeyProperty = context.ObjectType.GetProperty(
                publicKeyPropertyName
            );

            if (publicKeyProperty == null)
            {
                return false;
            }

            var publicKey = publicKeyProperty.GetValue(context.ObjectInstance);

            if (publicKey is not string)
            {
                return false;
            }

            var publicKeyObject = new EncryptionKeys((string)publicKey);
            var pgpBytes = Encoding.UTF8.GetBytes(message);
            using var stream = new MemoryStream(pgpBytes);
            using var pgp = new PGP(publicKeyObject);

            return pgp.VerifyClearStream(stream);
        }
        catch { }

        return false;
    }
}
