namespace HushWeb.Validators;

using System.ComponentModel.DataAnnotations;

/// <summary>
/// Validator used to check if input is a valid PGP message which was signed by
/// an administrator's PGP key.
/// </summary>
public class PgpSignedByAdmin : ValidationAttribute
{
    private const string BeginSignedMessage =
        "-----BEGIN PGP SIGNED MESSAGE-----";
    private const string BeginSignature = "-----BEGIN PGP SIGNATURE-----";
    private const string EndSignature = "-----END PGP SIGNATURE-----";

    /// <summary>
    /// Checks if input is a valid PGP message which was signed by an
    /// administrator's PGP key.
    /// </summary>
    protected override ValidationResult? IsValid(
        object? value,
        ValidationContext context
    )
    {
        var message = (string?)value;

        if (string.IsNullOrWhiteSpace(message))
        {
            return ValidationResult.Success;
        }

        message = message.Trim();

        if (
            !message.StartsWith(BeginSignedMessage)
            || !message.Contains(BeginSignature)
            || !message.EndsWith(EndSignature)
        )
        {
            return new ValidationResult(
                $"The {context.DisplayName} field is not a clear, PGP signed message"
            );
        }

        message = message.Trim();

        if (!Admin.HasSignedMessage(message))
        {
            return new ValidationResult(
                $"The {context.DisplayName} field is not signed by an administrator."
            );
        }

        return ValidationResult.Success;
    }
}
