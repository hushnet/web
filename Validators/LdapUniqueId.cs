namespace HushWeb.Validators;

using System.ComponentModel.DataAnnotations;

/// <summary>
/// Validator used to check check if the input is a safe LDAP unique ID.
/// </summary>
public class LdapUniqueId : ValidationAttribute
{
    private const string AllowedCharacters =
        "-_abcdefghijklmnopqrstuvwxyz1234567890";

    /// <summary>
    /// Checks if the input is a safe LDAP unique ID.
    /// </summary>
    protected override ValidationResult? IsValid(
        object? value,
        ValidationContext context
    )
    {
        var uniqueId = (string?)value;

        if (uniqueId == null)
        {
            return ValidationResult.Success;
        }

        var illegalCharacters = new List<char>();

        foreach (var character in uniqueId.ToLower())
        {
            if (
                !AllowedCharacters.Contains(character)
                && !illegalCharacters.Contains(character)
            )
            {
                illegalCharacters.Add(character);
            }
        }

        if (illegalCharacters.Count != 0)
        {
            var illegalCharsReadable = string.Join(
                ", ",
                illegalCharacters.Select(c => $"'{c}'")
            );
            return new ValidationResult(
                $"The {context.DisplayName} field contains illegal characters: {illegalCharsReadable}"
            );
        }

        return ValidationResult.Success;
    }
}
