namespace HushWeb.Validators;

using System.ComponentModel.DataAnnotations;
using HushWeb.Services.CryptoPermissions;

/// <summary>
/// Validator used to check if input contains a valid encoding of a <see cref="PermissionBlock"/>.
/// </summary>
public class ContainsPermissionBlock : ValidationAttribute
{
    /// <summary>
    /// Checks if input contains a valid encoding of a <see cref="PermissionBlock"/>.
    /// </summary>
    protected override ValidationResult? IsValid(
        object? value,
        ValidationContext context
    )
    {
        var message = (string?)value;

        if (message == null)
        {
            return ValidationResult.Success;
        }

        if (
            !message.Contains(PermissionBlock.BeginTag)
            || !message.Contains(PermissionBlock.EndTag)
        )
        {
            return new ValidationResult(
                $"The {context.DisplayName} field does not include a permission block"
            );
        }

        return ValidationResult.Success;
    }
}
