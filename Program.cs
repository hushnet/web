#pragma warning disable

using HushWeb;
using HushWeb.Services.ApplicationStore;
using HushWeb.Services.AuthenticationKeystore;
using HushWeb.Services.CryptoPermissions;
using HushWeb.Services.LdapUtils;
using HushWeb.Services.PgpKeystore;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.HttpOverrides;

#pragma warning restore

// * Important files and directories
const string DATA_PROTECTION_KEYS_DIR = "/app/keys";
const string ADMIN_PUBLIC_KEYS_DIR = "/app/admin/keys";
const string LDAP_PASSWORD_FILE = "/app/admin/secrets/openldap-passwd";
const string LDAP_TEMP_DIR = "/run/temp/ldif";
const string USER_APPLICATIONS_DIR = "/app/user-applications";
const string USER_PUBLIC_KEYS_DIR = "/app/user-public-keys";

// TODO: Make this a service
Admin.Initialize(
    adminPublicKeysDirectory: ADMIN_PUBLIC_KEYS_DIR,
    ldapPasswordFile: LDAP_PASSWORD_FILE,
    ldapDN: "cn=admin,dc=hush,dc=rip"
);

// * Built-in services
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews();
builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.ForwardedHeaders =
        ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
    options.AllowedHosts = new[] { "*" };
});
builder.Services
    .AddDataProtection()
    .SetApplicationName("hushweb")
    .SetDefaultKeyLifetime(TimeSpan.FromDays(90))
    .PersistKeysToFileSystem(new DirectoryInfo(DATA_PROTECTION_KEYS_DIR));
builder.Services.AddAntiforgery();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromHours(1);
});
builder.Services.AddHttpContextAccessor();

// * Custom services
builder.Services.AddSingleton<AuthenticationKeystoreService>();
builder.Services.AddSingleton<LdapUtilsService>(
    new LdapUtilsService(
        new()
        {
            Protocol = "ldap",
            Hostname = "openldap",
            Port = 1389,
            TemporaryFilesDirectory = LDAP_TEMP_DIR,
        }
    )
);
builder.Services.AddSingleton<CryptoPermissionsService>(
    provider =>
        new CryptoPermissionsService(
            provider.GetRequiredService<IDataProtectionProvider>(),
            new() { ProtectorPurpose = "CryptoPermissionsService", }
        )
);
builder.Services.AddSingleton<ApplicationStoreService>(
    provider =>
        new ApplicationStoreService(
            provider.GetRequiredService<CryptoPermissionsService>(),
            new()
            {
                ProtectorPurpose = "ApplicationStoreService",
                ApplicationFilesDirectory = USER_APPLICATIONS_DIR,
            }
        )
);
builder.Services.AddSingleton<PgpKeystoreService>(
    new PgpKeystoreService(new() { KeyFilesDirectory = USER_PUBLIC_KEYS_DIR })
);

// * Middleware
var app = builder.Build();
app.UseForwardedHeaders();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseStaticFiles();
app.UseHttpLogging();
app.UseStatusCodePages();
app.UseRouting();
app.UseSession();
app.UseAuthorization();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}"
);
app.Run("http://*:5000");
