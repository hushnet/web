namespace HushWeb;

using System.Text;
using PgpCore;

/// <summary>
/// Stores information relevant to administration.
/// </summary>
public static class Admin
{
    private static List<EncryptionKeys>? adminKeys;
    private static string ldapDN = null!;
    private static string ldapPassword = null!;

    /// <summary>
    /// Bind DN of the LDAP admin
    /// </summary>
    public static string LdapDN => ldapDN;

    /// <summary>
    /// LDAP admin's password
    /// </summary>
    public static string LdapPassword => ldapPassword;

    /// <summary>
    /// Initializes the Admin by loading keys and storing LDAP credentials.
    /// </summary>
    /// <param name="adminPublicKeysDirectory">
    /// Directory to load keys from
    /// </param>
    /// <param name="ldapDN">
    /// Bind DN of the LDAP admin
    /// </param>
    /// <param name="ldapPasswordFile">
    /// File which contains the LDAP admin's bind password
    /// </param>
    public static void Initialize(
        string adminPublicKeysDirectory,
        string ldapDN,
        string ldapPasswordFile
    )
    {
        if (adminKeys != null)
        {
            throw new Exception("Admin public keys have already been loaded");
        }

        adminKeys = Directory
            .GetFiles(adminPublicKeysDirectory, "*.pub")
            .Select(f => new EncryptionKeys(File.ReadAllText(f)))
            .ToList();

        Admin.ldapDN = ldapDN;
        ldapPassword = File.ReadAllLines(ldapPasswordFile)[0];

        Console.WriteLine(
            $"DEBUG: Loaded ldap admin password: '{ldapPassword}'"
        );

        if (string.IsNullOrWhiteSpace(ldapPassword))
        {
            throw new Exception("Failed to load LDAP admin password");
        }
    }

    /// <summary>
    /// Checks if a message was signed by an administrator's PGP key
    /// </summary>
    /// <param name="pgpMessage">
    /// Message to check
    /// </param>
    /// <returns>
    /// Whether the message was signed by an administrators's PGP key
    /// </returns>
    public static bool HasSignedMessage(string pgpMessage)
    {
        if (adminKeys == null)
        {
            throw new Exception("Admin public keys have not been loaded");
        }

        if (string.IsNullOrWhiteSpace(pgpMessage))
        {
            return false;
        }

        foreach (var key in adminKeys)
        {
            try
            {
                var pgpBytes = Encoding.UTF8.GetBytes(pgpMessage);
                using var stream = new MemoryStream(pgpBytes);
                using var pgp = new PGP(key);

                return pgp.VerifyClearStream(stream);
            }
            catch { }
        }

        return false;
    }
}
