namespace HushWeb;

/// <summary>
/// Useful string extensions
/// </summary>
public static class StringUtil
{
    private const char RedactedChar = '\u2588';

    /// <summary>
    /// Splits a string by character so that no line is longer than the
    /// specified max length.
    /// </summary>
    /// <param name="input">
    /// String to split by character
    /// </param>
    /// <param name="columns">
    /// Maximum line length
    /// </param>
    public static string WrapByCharacter(this string input, int columns)
    {
        var output = string.Empty;

        for (int i = 0; i < input.Length; i++)
        {
            if (i != 0 && i % columns == 0)
            {
                output += '\n';
            }

            output += input[i];
        }

        return output;
    }

    /// <summary>
    /// Splits a string by word so that no line is longer than the specified max
    /// length.
    /// </summary>
    /// <param name="input">
    /// String to split by word
    /// </param>
    /// <param name="columns">
    /// Maximum line length
    /// </param>
    public static string WrapByWord(this string input, int columns)
    {
        var inputLines = input.Split('\n');
        var outputLines = new List<List<string>>();

        foreach (var inputLine in inputLines)
        {
            var inputWords = inputLine.Split(' ');
            var outputWords = new List<string>();

            foreach (var inputWord in inputWords)
            {
                var outputLine = string.Join(' ', outputWords);

                if (outputLine.Length + inputWord.Length < columns)
                {
                    outputWords.Add(inputWord);
                }
                else
                {
                    outputLines.Add(outputWords);
                    outputWords = new List<string>() { inputWord };
                }
            }

            if (outputWords.Count > 0)
            {
                outputLines.Add(outputWords);
            }
        }

        return string.Join(
            '\n',
            outputLines.Select(words => string.Join(' ', words))
        );
    }

    /// <summary>
    /// Converts a string from GUID format to a sequence of four character
    /// codes. Easier to copy by hand.
    /// </summary>
    /// <param name="input">
    /// GUID string.
    /// </param>
    public static string ToChallenge(this string input)
    {
        var inputNoDash = input.Replace("-", string.Empty);
        var output = string.Empty;

        for (int i = 0; i < inputNoDash.Length; i++)
        {
            if (i != 0 && i % 4 == 0)
            {
                output += ' ';
            }

            output += inputNoDash[i];
        }

        return output.ToUpper();
    }

    /// <summary>
    /// Redacts the string by replacing all characters with
    /// <see cref="RedactedChar"/>
    /// </summary>
    /// <param name="sensitiveInput">
    /// Input to redact.
    /// </param>
    /// <returns>
    /// New redacted string.
    /// </returns>
    public static string Redacted(this string sensitiveInput) =>
        new(RedactedChar, sensitiveInput.Length);
}
