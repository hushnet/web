# Software Recommendations

## PGP

See also: [GnuPG Frontends](https://www.gnupg.org/software/frontends.html)
### Graphical
- [Gnu Privacy Assistant](https://gnupg.org/software/gpa/index.html)
  - License: [GNU GPL 3](https://github.com/gpg/gpa/blob/master/COPYING)
  - Platform(s): Linux
- [Gpg4win](https://www.gpg4win.org)
  - License: [Various free licenses, mostly GNU GPL](https://www.gpg4win.org/license.html)
  - Platform(s): Windows
### Terminal
- [GPG](https://gnupg.org)
  - License: [GNU GPL 3](https://dev.gnupg.org/source/gnupg/browse/master/COPYING)
  - Platform(s): Linux, Windows, Mac

## XMPP

See also: [XMPP Clients](https://xmpp.org/software/clients)

XMPP is designed with graphical clients in mind. XMPP is useable in the terminal
but many important features inherently require graphics, such as profile
pictures and media sharing.

⚠️ AstraChat is proprietary software with government ties. Don't use it!

### Graphical (Recommended)
- [Gajim](https://gajim.org)
  - License: [GNU GPL 3](https://dev.gajim.org/gajim/gajim/-/blob/master/COPYING)
  - Platform(s): Linux, Windows, Mac
- [Conversations](https://f-droid.org/packages/eu.siacs.conversations)
  - License: [GNU GPL 3](https://github.com/iNPUTmice/Conversations/blob/master/LICENSE)
  - Platform(s): Android
### Terminal
- [Profanity](https://profanity-im.github.io)
  - License: [GNU GPL 3](https://github.com/profanity-im/profanity/blob/master/COPYING)
  - Platform(s): Linux, Windows, Mac