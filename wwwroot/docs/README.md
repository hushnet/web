# About `Docs`

This section contains information on a variety of topics, including (but not
limited to):
- Interacting with our services
- Using free software
- Privacy and security

# Contributing
To add a document or make changes to an existing document, you can
[submit a merge request][mr]. Documents are served from [`wwwroot/docs`][wd],
and all documents should be in a logical subdirectory. If you create a new
subdirectory, it should contain a `README.md` describing its purpose.

## Writing style
Please do your best to ensure the quality of your submission by running it
through a [writing assistant][wa]. If your submission contains errors in
spelling, grammar, punctuation, or clarity, your submission will need to be
amended. Your writing should aim to be [expository][ex], and opinions should be
clearly labeled as such.

## Format
When submitting changes to an existing document, make sure your changes are
consistent with the format of the document. When submitting a new document, you
are permitted to take liberties with the format, so long as it's consistent.

### Guidelines
- Lines should be kept under 80 lines whenever possible.
- Large documents should include a table of contents with jump links.
- Avoid referencing external assets unless necessary.
- Prefer [reference-style links][rl] to keep the document source clean.

## Rules
- Submissions must be in accordance with [User Agreement II-IV][ua].
- Do not recommend the use of proprietary software.

[mr]: https://gitlab.com/hushnet/web/-/merge_requests/new
[wd]: https://gitlab.com/hushnet/web/-/tree/main/wwwroot/docs
[ua]: User_Agreement.txt
[wa]: https://grammarly.com
[ex]: https://www.grammarly.com/blog/expository-writing
[rl]: https://www.markdownguide.org/basic-syntax/#reference-style-links