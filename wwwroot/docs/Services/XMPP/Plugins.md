# Openfire (XMPP) Plugins
See also: [Openfire plugin list][pl]

# Plugins
## HTTP File Upload
- Config: `Server > Server Settings > HTTP Binding`
- Description: Allows clients to share files, as described in the XEP-0363
  'HTTP File Upload' specification.
- Author: Guus der Kinderen

## MotD (Message of the Day)
- Config: `Users/Groups > Users > MotD Properties`
- Description: Allows admins to have a message sent to users each time they log
  in.
- Author: Ryan Graham

## Push Notification
- Config: `Server >  Server Manager > System Properties`
- Description: Adds Push Notification (XEP-0357) support to Openfire.
- Author: Guus der Kinderen

## Search
- Config: `Users/Groups > Users > Advanced User Search`
- Description: Provides support for Jabber Search. (XEP-0055)
- Author: Ryan Graham

## Subscription
- Config: `Server > Server Settings > Subscription Properties`
- Description: Automatically accepts or rejects subscription requests.
- Author: Ryan Graham

## Broadcast
Administrators can broadcast to all users by sending a message to
`all@broadcast.hush.rip`.
- Config: `Server >  Server Manager > System Properties`
- Description: Broadcasts messages to all users in the system or to specific
  groups.
- Author: Ignite Realtime

## Monitoring Service
Privacy-compromising features have been disabled. This plugin is only
used to enable message archiving as per [XEP-0313][ma]. Archived messages are
deleted after 7 days.
- Config: `Server > Archiving`
- Description: Monitors conversations and statistics of the server.
- Author: Ignite Realtime

[pl]: https://www.igniterealtime.org/projects/openfire/plugins.jsp
[ma]: https://xmpp.org/extensions/xep-0313.html