# XMPP - Instant Messaging

See also: [Openfire homepage][oh], [An Overview of XMPP][ox]

> XMPP is the Extensible Messaging and Presence Protocol, a set of open
> technologies for instant messaging, presence, multi-party chat, voice and
> video calls, collaboration, lightweight middleware, content syndication, and
> generalized routing of XML data.

We use Openfire to provide a secure and private channel for real-time
communication.

[ox]: https://xmpp.org/about/technology-overview
[oh]: https://www.igniterealtime.org/projects/openfire/index.jsp