# About `Services`

This directory relates to the services offered by this platform. Topics include:
- Description of offered services
- How to interact with them privately and securely
- Back-end software used, and how it's configured

## Tech Stack

See also: [`docker-compose.yaml`][dc]

- LDAP: [OpenLDAP][ol]
- XMPP: [Openfire][of]
- Database: [PostgresSQL][pg]
- Web: [ASP.NET Core][an]
- Proxy: [NGINX][ng]

## Planned
- Forums: [phpBB][bb]

[dc]: https://gitlab.com/hushnet/compose/-/blob/main/docker-compose.yaml
[ol]: https://openldap.org
[of]: https://www.igniterealtime.org/projects/openfire
[pg]: https://www.postgresql.org
[an]: https://docs.microsoft.com/en-us/aspnet/core/introduction-to-aspnet-core
[ng]: https://www.nginx.com/
[bb]: https://www.phpbb.com/