namespace HushWeb.Services.PgpKeystore;

/// <summary>
/// Options for PgpKeystore
/// </summary>
public struct PgpKeystoreOptions
{
    /// <summary>
    /// Directory to store users' PGP public keys.
    /// </summary>
    public string KeyFilesDirectory { get; init; }
}
