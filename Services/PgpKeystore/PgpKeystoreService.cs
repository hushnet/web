namespace HushWeb.Services.PgpKeystore;

using PgpCore;

/// <summary>
/// Handles storage and access for users' PGP public keys.
/// </summary>
public class PgpKeystoreService
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PgpKeystoreService"/> class.
    /// </summary>
    /// <param name="options">
    /// Initialization options.
    /// </param>
    public PgpKeystoreService(PgpKeystoreOptions options)
    {
        KeyFilesDirectory = options.KeyFilesDirectory;

        if (!Directory.Exists(KeyFilesDirectory))
        {
            Directory.CreateDirectory(KeyFilesDirectory);
        }
    }

    /// <summary>
    /// Directory to store users' PGP public keys.
    /// </summary>
    public string KeyFilesDirectory { get; init; } = null!;

    /// <summary>
    /// Tests if a string is a valid PGP public key. Throws an exception if
    /// invalid.
    /// </summary>
    /// <param name="key">
    /// String to test.
    /// </param>
    public static void AssertKeyValid(string key)
    {
        using var pgp = new PGP(new EncryptionKeys(key));
        pgp.EncryptArmoredString("TEST");
    }

    /// <summary>
    /// Stores a user's public key.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the key's owner.
    /// </param>
    /// <param name="key">
    /// Key to store.
    /// </param>
    public async Task<bool> StoreKey(string uniqueId, string key)
    {
        try
        {
            // Validation should be handled by a validator attribute in the
            // input model. This should never throw but it's important that we
            // check anyway. Don't want to write a malformed or malicious file.
            AssertKeyValid(key);
            var filePath = KeyPath(uniqueId);
            await File.WriteAllTextAsync(filePath, key);
            File.SetAttributes(filePath, FileAttributes.ReadOnly);
        }
        catch
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Looks up and reads a user's stored key.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the key's owner.
    /// </param>
    /// <returns>
    /// User's PGP public key, if it exists.
    /// </returns>
    public async Task<string?> GetKey(string uniqueId)
    {
        if (!UserHasKey(uniqueId))
        {
            return null;
        }

        return await File.ReadAllTextAsync(KeyPath(uniqueId));
    }

    /// <summary>
    /// Deletes a user's stored key.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the key's owner.
    /// </param>
    /// <returns>
    /// Whether the key was deleted.
    /// </returns>
    public bool DeleteKey(string uniqueId)
    {
        if (!UserHasKey(uniqueId))
        {
            return false;
        }

        var filePath = KeyPath(uniqueId);
        File.SetAttributes(filePath, FileAttributes.Normal);
        File.Delete(filePath);
        return true;
    }

    /// <summary>
    /// Determines if a user has a key stored.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the user to lookup keys for.
    /// </param>
    /// <returns>
    /// Whether the user has a key stored.
    /// </returns>
    public bool UserHasKey(string uniqueId) => File.Exists(KeyPath(uniqueId));

    /// <summary>
    /// Interpolates the path to a user's key.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the user to interpolate the key path for.
    /// </param>
    public string KeyPath(string uniqueId) =>
        Path.Join(KeyFilesDirectory, $"{uniqueId}.pub");
}
