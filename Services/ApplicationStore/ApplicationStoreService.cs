namespace HushWeb.Services.ApplicationStore;

using HushWeb.Models;
using HushWeb.Services.CryptoPermissions;
using Microsoft.AspNetCore.DataProtection;
using Newtonsoft.Json;
using PgpCore;

/// <summary>
/// Handles storage and access for users' applications.
/// </summary>
public class ApplicationStoreService
{
    private readonly CryptoPermissionsService cryptoPermissions;

    /// <summary>
    /// Initializes a new instance of the <see cref="ApplicationStoreService"/>
    /// class.
    /// </summary>
    /// <param name="cryptoPermissions">
    /// Service used to interact with permission blocks.
    /// </param>
    /// <param name="options">
    /// Initialization options.
    /// </param>
    public ApplicationStoreService(
        CryptoPermissionsService cryptoPermissions,
        ApplicationStoreOptions options
    )
    {
        this.cryptoPermissions = cryptoPermissions;
        ApplicationFilesDirectory = options.ApplicationFilesDirectory;

        if (!Directory.Exists(ApplicationFilesDirectory))
        {
            Directory.CreateDirectory(ApplicationFilesDirectory);
        }
    }

    /// <summary>
    /// Directory to store users' applications.
    /// </summary>
    public string ApplicationFilesDirectory { get; init; } = null!;

    /// <summary>
    /// Stores a user's application.
    /// </summary>
    /// <param name="application">
    /// Application to store.
    /// </param>
    public async Task<(bool success, string? reason)> StoreApplication(
        ApplicationForm application
    )
    {
        try
        {
            if (UserHasApplication(application.UniqueId))
            {
                return (
                    false,
                    "An application already exists with the specified username."
                );
            }

            var permissionBlock = new CreateAccountPermissionBlock(application);
            var filePath = ApplicationPath(application.UniqueId);

            await File.WriteAllTextAsync(
                filePath,
                cryptoPermissions.RenderBlock(permissionBlock)
            );

            File.SetAttributes(filePath, FileAttributes.ReadOnly);
        }
        catch (Exception e)
        {
            return (false, e.Message);
        }

        return (true, null);
    }

    /// <summary>
    /// Looks up and reads a user's application.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the applicant.
    /// </param>
    /// <returns>
    /// User's application, if it exists.
    /// </returns>
    public async Task<string> GetApplication(string uniqueId)
    {
        if (!UserHasApplication(uniqueId))
        {
            throw new Exception("Application does not exist");
        }

        var application = await File.ReadAllTextAsync(
            ApplicationPath(uniqueId)
        );

        return application.Trim();
    }

    /// <summary>
    /// Gets the Unique IDs associated with all pending applications.
    /// </summary>
    public async Task<List<string>> GetAllApplicantUniqueIds()
    {
        return await Task.Run(
            () =>
                Directory
                    .GetFiles(ApplicationFilesDirectory)
                    .Select(f => Path.GetFileName(f))
                    .ToList()
        );
    }

    /// <summary>
    /// Deletes a user's application.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the applicant.
    /// </param>
    /// <returns>
    /// Whether the application was deleted.
    /// </returns>
    public bool DeleteApplication(string uniqueId)
    {
        if (!UserHasApplication(uniqueId))
        {
            return false;
        }

        var filePath = ApplicationPath(uniqueId);
        File.SetAttributes(filePath, FileAttributes.Normal);
        File.Delete(filePath);
        return true;
    }

    /// <summary>
    /// Determines if a user has an application stored.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the user to lookup applications for.
    /// </param>
    /// <returns>
    /// Whether the user has a application stored.
    /// </returns>
    public bool UserHasApplication(string uniqueId) =>
        File.Exists(ApplicationPath(uniqueId));

    /// <summary>
    /// Interpolates the path to a user's application.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the user to interpolate the application path for.
    /// </param>
    public string ApplicationPath(string uniqueId) =>
        Path.Join(ApplicationFilesDirectory, uniqueId);
}
