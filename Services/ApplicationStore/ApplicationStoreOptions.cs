namespace HushWeb.Services.ApplicationStore;

/// <summary>
/// Options for <see cref="ApplicationStoreService"/>.
/// </summary>
public struct ApplicationStoreOptions
{
    /// <summary>
    /// Purpose to specify for the protector use.
    /// </summary>
    public string ProtectorPurpose { get; init; }

    /// <summary>
    /// Directory to store users' applications.
    /// </summary>
    public string ApplicationFilesDirectory { get; init; }
}
