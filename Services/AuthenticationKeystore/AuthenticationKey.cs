namespace HushWeb.Services.AuthenticationKeystore;

/// <summary>
/// Used as single-use authentication for account-related actions. The key is
/// meant to be stored on both the client and server, and a lookup is performed
/// to check authenticity.
/// </summary>
public struct AuthenticationKey
{
    /// <summary>
    /// How long the key should be valid.
    /// </summary>
    public static readonly TimeSpan Duration = TimeSpan.FromMinutes(15);

    /// <summary>
    /// Initializes a new instance of the <see cref="AuthenticationKey"/> struct.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of the key's owner
    /// </param>
    /// <param name="password">
    /// Password of the key's owner
    /// </param>
    public AuthenticationKey(string uniqueId, string password)
    {
        Expiry = DateTime.Now.Add(Duration);
        Token = Guid.NewGuid().ToString();
        UniqueId = uniqueId;
        Password = password;
    }

    /// <summary>
    /// When the key expires.
    /// </summary>
    public DateTime Expiry { get; set; }

    /// <summary>
    /// Guaranteed unique token to prevent reusing keys.
    /// </summary>
    public string Token { get; set; }

    /// <summary>
    /// Unique ID of the key's owner.
    /// </summary>
    public string UniqueId { get; set; }

    /// <summary>
    /// Password of the key's owner.
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    /// Determines if the key is expired.
    /// </summary>
    public bool IsExpired => DateTime.Now > Expiry;
}
