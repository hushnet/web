namespace HushWeb.Services.AuthenticationKeystore;

/// <summary>
/// Service used to manage authentication key storage and lookup.
/// </summary>
/// <remarks>
/// Example: Key storage
/// <c>
/// var key = new AuthenticationKey(uniqueId, password);
/// authKeystore.RemoveExpired();
/// authKeystore.RemoveWithUniqueId(uniqueId);
/// authKeystore.StoreKey(key);
/// </c>
/// </remarks>
public class AuthenticationKeystoreService
{
    private readonly Dictionary<string, AuthenticationKey> keys;

    /// <summary>
    /// Initializes a new instance of the <see cref="AuthenticationKeystoreService"/> class.
    /// </summary>
    public AuthenticationKeystoreService() => keys = new();

    /// <summary>
    /// Adds a key to the keys dictionary.
    /// </summary>
    /// <param name="key">
    /// Key to
    /// </param>
    public void StoreKey(AuthenticationKey key) => keys[key.Token] = key;

    /// <summary>
    /// Checks for a key with the specified details.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID that corresponds to the key.
    /// </param>
    /// <param name="token">
    /// Token that corresponds to the key.
    /// </param>
    /// <returns>
    /// The key, if it exists.
    /// </returns>
    public AuthenticationKey? GetKey(string uniqueId, string token)
    {
        if (!keys.ContainsKey(token))
        {
            return null;
        }

        var key = keys[token];

        if (key.IsExpired || key.UniqueId != uniqueId)
        {
            return null;
        }

        return key;
    }

    /// <summary>
    /// Removes all expired keys.
    /// </summary>
    public void RemoveExpired()
    {
        foreach (var kv in keys.Where(kv => kv.Value.IsExpired))
        {
            keys.Remove(kv.Key);
        }
    }

    /// <summary>
    /// Removes all authentication keys for one user.
    /// </summary>
    /// <param name="uniqueId">
    /// Unique ID of user whose keys should be removed.
    /// </param>
    public void RemoveWithUniqueId(string uniqueId)
    {
        foreach (var kv in keys.Where(kv => kv.Value.UniqueId == uniqueId))
        {
            keys.Remove(kv.Key);
        }
    }
}
