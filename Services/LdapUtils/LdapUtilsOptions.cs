namespace HushWeb.Services.LdapUtils;

/// <summary>
/// Initialization options for the <see cref="LdapUtilsService"/>
/// </summary>
public struct LdapUtilsOptions
{
    /// <summary>
    /// Protocol to use when connecting to the LDAP server.
    /// </summary>
    public string Protocol { get; init; }

    /// <summary>
    /// Hostname of the LDAP server.
    /// </summary>
    public string Hostname { get; init; }

    /// <summary>
    /// Port of the LDAP server.
    /// </summary>
    public int Port { get; init; }

    /// <summary>
    /// Directory to store temporary files in.
    /// </summary>
    public string TemporaryFilesDirectory { get; init; }
}
