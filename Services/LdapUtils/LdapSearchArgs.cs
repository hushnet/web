namespace HushWeb.Services.LdapUtils;

/// <summary>
/// Argument generator for ldapsearch command.
/// </summary>
public static class LdapSearchArgs
{
    private static readonly string[] ValidScopes = new[]
    {
        "sub",
        "base",
        "one",
        "children",
    };

    /// <summary>
    /// Search base argument for ldapsearch.
    /// </summary>
    /// <param name="searchBase">
    /// Search base.
    /// </param>
    public static string Base(string searchBase) => $"-b {searchBase}";

    /// <summary>
    /// Scope argument for ldapsearch.
    /// </summary>
    /// <param name="scope">
    /// Search scope: base, one, sub, children.
    /// </param>
    public static string Scope(string scope)
    {
        if (!ValidScopes.Contains(scope))
        {
            throw new Exception($"Invalid search scope: '{scope}'");
        }

        return $"-s {scope}";
    }
}
