namespace HushWeb.Services.LdapUtils;

#pragma warning disable

/// <summary>
/// LDAP change types for use with ldapmodify.
/// </summary>
public static class LdapChangeType
{
    public static readonly string Add = "add";
    public static readonly string Replace = "replace";
    public static readonly string Delete = "delete";
}
