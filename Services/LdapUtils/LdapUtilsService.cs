namespace HushWeb.Services.LdapUtils;

using System.Diagnostics;
using System.Net;
using Newtonsoft.Json;

/// <summary>
/// Service used to interact with the LDAP server.
/// </summary>
public class LdapUtilsService
{
    /// <summary>
    /// Initializes a new instance of the <see cref="LdapUtilsService"/> class.
    /// </summary>
    /// <param name="options">
    /// Initialization options
    /// </param>
    public LdapUtilsService(LdapUtilsOptions options)
    {
        var ip = GetIpAddress(options.Hostname);
        Url = $"{options.Protocol}://{ip}:{options.Port}";
        TemporaryFilesDirectory = options.TemporaryFilesDirectory;
    }

    /// <summary>
    /// URL of the LDAP server. Includes protocol, hostname, and port.
    /// </summary>
    public string Url { get; init; } = null!;

    /// <summary>
    /// Directory to store temporary files in.
    /// </summary>
    public string TemporaryFilesDirectory { get; init; } = null!;

    /// <summary>
    /// Executes an ldapsearch anonymously and converts the resultant LDIF to a
    /// model. See:
    /// <see href="https://confluence.atlassian.com/kb/how-to-write-ldap-search-filters-792496933.html">Atlassian article</see>
    /// for details on how to write an LDAP search filter.
    /// </summary>
    /// <param name="searchBase">
    /// Starting point of the search.
    /// </param>
    /// <param name="scope">
    /// Search scope. {base|one|sub|children}
    /// </param>
    /// <param name="filter">
    /// Search filter.
    /// </param>
    /// <typeparam name="T">
    /// Model to convert the LDIF to.
    /// </typeparam>
    public async Task<Dictionary<string, T>> SearchAnonymous<T>(
        string searchBase,
        string scope = "sub",
        string filter = ""
    )
    {
        var result = await ExecuteCommandAsync(
            LdapCommand.Search,
            "-LLL",
            LdapArgs.Host(Url),
            LdapArgs.AuthSimple(),
            LdapSearchArgs.Base(searchBase),
            LdapSearchArgs.Scope(scope),
            filter
        );

        return await ConvertLdifToJson<T>(result.Output);
    }

    /// <summary>
    /// Executes an ldapsearch and converts the resultant LDIF to a model.
    /// </summary>
    /// <param name="bindDN">
    /// DN of the searcher.
    /// </param>
    /// <param name="searchBase">
    /// Starting point of the search.
    /// </param>
    /// <param name="password">
    /// Searcher's password.
    /// </param>
    /// <param name="scope">
    /// Search scope. {base|one|sub|children}
    /// </param>
    /// <param name="filter">
    /// Search filter.
    /// </param>
    /// <typeparam name="T">
    /// Model to convert the LDIF to.
    /// </typeparam>
    public async Task<Dictionary<string, T>> Search<T>(
        string bindDN,
        string searchBase,
        string password = "",
        string scope = "sub",
        string filter = ""
    )
    {
        var result = await SearchRaw(
            bindDN,
            searchBase,
            password,
            scope,
            filter
        );
        return await ConvertLdifToJson<T>(result.Output);
    }

    /// <summary>
    /// Executes an ldapsearch command.
    /// </summary>
    /// <param name="bindDN">
    /// DN of the searcher.
    /// </param>
    /// <param name="searchBase">
    /// Starting point of the search.
    /// </param>
    /// <param name="password">
    /// Searcher's password.
    /// </param>
    /// <param name="scope">
    /// Search scope. {base|one|sub|children}
    /// </param>
    /// <param name="filter">
    /// Search filter.
    /// </param>
    public async Task<CommandResult> SearchRaw(
        string bindDN,
        string searchBase,
        string password = "",
        string scope = "sub",
        string filter = ""
    )
    {
        return await ExecuteCommandAsync(
            LdapCommand.Search,
            "-LLL",
            LdapArgs.Host(Url),
            LdapArgs.AuthSimple(),
            LdapArgs.BindDN(bindDN),
            LdapArgs.Password(password),
            LdapSearchArgs.Base(searchBase),
            LdapSearchArgs.Scope(scope),
            filter
        );
    }

    /// <summary>
    /// Executes an ldapmodify command.
    /// </summary>
    /// <param name="bindDN">
    /// DN of the modifier (admin).
    /// </param>
    /// <param name="password">
    /// Password of the modifier (admin).
    /// </param>
    /// <param name="ldif">
    /// LDIF of the changes to make.
    /// </param>
    public async Task<CommandResult> Modify(
        string bindDN,
        string password,
        string ldif
    )
    {
        using var file = new TemporaryFile(ldif, TemporaryFilesDirectory);
        return await ExecuteCommandAsync(
            LdapCommand.Modify,
            LdapArgs.Host(Url),
            LdapArgs.AuthSimple(),
            LdapArgs.BindDN(bindDN),
            LdapArgs.Password(password),
            LdapArgs.File(file.Path)
        );
    }

    /// <summary>
    /// Executes an ldapadd command.
    /// </summary>
    /// <param name="bindDN">
    /// DN of the adder (admin).
    /// </param>
    /// <param name="password">
    /// Password of the adder (admin).
    /// </param>
    /// <param name="ldif">
    /// LDIF of the additions to make.
    /// </param>
    public async Task<CommandResult> Add(
        string bindDN,
        string password,
        string ldif
    )
    {
        using var file = new TemporaryFile(ldif, TemporaryFilesDirectory);
        return await ExecuteCommandAsync(
            LdapCommand.Add,
            LdapArgs.Host(Url),
            LdapArgs.AuthSimple(),
            LdapArgs.BindDN(bindDN),
            LdapArgs.Password(password),
            LdapArgs.File(file.Path)
        );
    }

    /// <summary>
    /// Executes an ldapdelete command.
    /// </summary>
    /// <param name="bindDN">
    /// DN of the deleter (admin).
    /// </param>
    /// <param name="password">
    /// Password of the deleter (admin).
    /// </param>
    /// <param name="deleteDN">
    /// DN of the entry to delete.
    /// </param>
    public async Task<CommandResult> Delete(
        string bindDN,
        string password,
        string deleteDN
    )
    {
        return await ExecuteCommandAsync(
            LdapCommand.Delete,
            LdapArgs.Host(Url),
            LdapArgs.AuthSimple(),
            LdapArgs.BindDN(bindDN),
            LdapArgs.Password(password),
            deleteDN
        );
    }

    /// <summary>
    /// Executes an ldappasswd command.
    /// </summary>
    /// <param name="bindDN">
    /// DN of the password changer (admin).
    /// </param>
    /// <param name="adminPassword">
    /// Admin password.
    /// </param>
    /// <param name="userDN">
    /// DN of the password changee.
    /// </param>
    /// <param name="userNewPassword">
    /// Password changee's new password.
    /// </param>
    public async Task<CommandResult> Password(
        string bindDN,
        string adminPassword,
        string userDN,
        string userNewPassword
    )
    {
        return await ExecuteCommandAsync(
            LdapCommand.Password,
            LdapArgs.Host(Url),
            LdapArgs.AuthSimple(),
            LdapArgs.BindDN(bindDN),
            LdapArgs.Password(adminPassword),
            LdapPasswordArgs.NewPassword(userNewPassword),
            userDN
        );
    }

    private static string GetIpAddress(string hostname)
    {
        var hostEntry = Dns.GetHostEntry(hostname);

        if (hostEntry.AddressList.Length == 0)
        {
            throw new Exception(
                $"Failed to resolve LDAP hostname: '${hostname}'"
            );
        }

        return hostEntry.AddressList[0].ToString();
    }

    private static CommandResult ExecuteCommand(
        string command,
        params string[] args
    )
    {
        var process = Process.Start(
            new ProcessStartInfo()
            {
                FileName = command,
                Arguments = string.Join(' ', args),
                UseShellExecute = false,
                RedirectStandardOutput = true,
            }
        );

        if (process == null)
        {
            throw new Exception($"Failed to execute command: '{command}'");
        }

        var output = process.StandardOutput.ReadToEnd();
        process.WaitForExit();

        return new(output, process.ExitCode);
    }

    private static Task<CommandResult> ExecuteCommandAsync(
        string command,
        params string[] args
    )
    {
        return Task.Run(() => ExecuteCommand(command, args));
    }

    private Task<Dictionary<string, T>> ConvertLdifToJson<T>(string ldif)
    {
        return Task.Run(() =>
        {
            string json = ConvertLdifToJsonRaw(ldif);

            var objArr = JsonConvert.DeserializeObject<Dictionary<string, T>>(
                json
            );

            if (objArr == null)
            {
                throw new Exception($"Failed to parse LDIF into {typeof(T)}[]");
            }

            return objArr;
        });
    }

    private string ConvertLdifToJsonRaw(string ldif)
    {
        using var file = new TemporaryFile(ldif, TemporaryFilesDirectory);
        var script = "/app/python/ldif2json.py";
        var result = ExecuteCommand("python3", script, file.Path);
        return result.Output;
    }
}
