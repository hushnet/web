namespace HushWeb.Services.LdapUtils;

/// <summary>
/// Argument generator for general use with ldap-util commands.
/// </summary>
public static class LdapArgs
{
    /// <summary>
    /// Host argument.
    /// </summary>
    public static string Host(string url) => $"-H {url}";

    /// <summary>
    /// Simple authentication argument.
    /// </summary>
    public static string AuthSimple() => $"-x";

    /// <summary>
    /// Bind DN argument.
    /// </summary>
    public static string BindDN(string dn) => $"-D {dn}";

    /// <summary>
    /// Bind password argument.
    /// </summary>
    public static string Password(string password) => $"-w {password}";

    /// <summary>
    /// LDIF file argument.
    /// </summary>
    public static string File(string filePath) => $"-f {filePath}";
}
