namespace HushWeb.Services.LdapUtils;

#pragma warning disable

/// <summary>
/// List of ldap-utils command names. See: <see href="https://wiki.debian.org/LDAP/LDAPUtils">ldap-utils Debian wiki page.</see>
/// </summary>
public static class LdapCommand
{
    public static readonly string Search = "ldapsearch";
    public static readonly string Modify = "ldapmodify";
    public static readonly string Add = "ldapadd";
    public static readonly string Delete = "ldapdelete";
    public static readonly string Rename = "ldapmodrdn";
    public static readonly string Password = "ldappasswd";
    public static readonly string WhoAmI = "ldapwhoami";
    public static readonly string Compare = "ldapcompare";
}
