namespace HushWeb.Services.LdapUtils;

/// <summary>
/// Argument generator for ldappasswd command.
/// </summary>
public static class LdapPasswordArgs
{
    /// <summary>
    /// New password file argument for ldappasswd.
    /// </summary>
    public static string NewPasswordFile(string passwordFile) =>
        $"-T {passwordFile}";

    /// <summary>
    /// New password argument for ldappasswd.
    /// </summary>
    public static string NewPassword(string password) =>
        $"-s '{password.Replace("'", string.Empty)}'";
}
