namespace HushWeb.Services.LdapUtils;

/// <summary>
/// The result of a shell command that was executed programmatically.
/// </summary>
public struct CommandResult
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CommandResult"/> struct.
    /// </summary>
    /// <param name="output">
    /// The command's output, captured from stdout.
    /// </param>
    /// <param name="exitCode">
    /// The command's exit code.
    /// </param>
    public CommandResult(string output, int exitCode)
    {
        Output = output;
        ExitCode = exitCode;
    }

    /// <summary>
    /// The command's output, captured from stdout.
    /// </summary>
    public string Output { get; set; }

    /// <summary>
    /// The command's exit code. See:
    /// <see href="https://ldapwiki.com/wiki/LDAP%20Result%20Codes">
    /// Ldapwiki
    /// </see>
    /// for information on possible exit codes.
    /// </summary>
    public int ExitCode { get; set; }
}
