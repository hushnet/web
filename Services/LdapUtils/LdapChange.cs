namespace HushWeb.Services.LdapUtils;

using System.Text;

/// <summary>
/// Represents one change to an LDAP object.
/// </summary>
public struct LdapChange
{
    /// <summary>
    /// What type of change should be made.
    /// </summary>
    public string ChangeType { get; init; }

    /// <summary>
    /// Name of the attribute to change.
    /// </summary>
    public string Attribute { get; init; }

    /// <summary>
    /// New value of the attribute.
    /// </summary>
    public string Value { get; init; }

    /// <summary>
    /// Whether the value should be represented as Base64 in the LDIF.
    /// </summary>
    public bool ConvertToBase64 { get; init; }

    /// <inheritdoc/>
    public override string ToString()
    {
        var valueEncoded = Value;

        if (ConvertToBase64)
        {
            var bytes = Encoding.UTF8.GetBytes(Value);
            var base64 = Convert.ToBase64String(bytes);
            valueEncoded = string.Empty;

            // Split base64 string into multiple lines
            for (int i = 0; i < base64.Length; i++)
            {
                var col = i + Attribute.Length + 3;
                if (col != 0 && col % 64 == 0)
                {
                    valueEncoded += "\n ";
                }

                valueEncoded += base64[i];
            }
        }

        var output = ChangeType;
        output += ": ";
        output += Attribute;
        output += '\n';
        output += Attribute;
        output += ConvertToBase64 ? ":: " : ": ";
        output += valueEncoded;
        return output;
    }
}
