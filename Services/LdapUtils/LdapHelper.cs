namespace HushWeb.Services.LdapUtils;

using AntiLdapInjection;
using HushWeb.Models;

/// <summary>
/// LDAP-related helper functions.
/// </summary>
public static class LdapHelper
{
    /// <summary>
    /// Sanitizes an ldapsearch filter.
    /// </summary>
    /// <param name="input">
    /// Search filter to sanitize.
    /// </param>
    public static string EncodeFilter(string input) =>
        LdapEncoder.FilterEncode(input);

    /// <summary>
    /// Sanitizes a DN.
    /// </summary>
    /// <param name="input">
    /// DN to sanitize.
    /// </param>
    public static string EncodeDN(string input) =>
        LdapEncoder.DistinguishedNameEncode(input);

    /// <summary>
    /// Tests user credentials to see if they match an existing account.
    /// </summary>
    /// <param name="ldapService">
    /// LdapUtilsService to execute the command.
    /// </param>
    /// <param name="uniqueId">
    /// Unique ID to test.
    /// </param>
    /// <param name="password">
    /// Password to test.
    /// </param>
    /// <returns>
    /// Whether the credentials are valid.
    /// </returns>
    public static async Task<bool> AuthenticateUser(
        LdapUtilsService ldapService,
        string uniqueId,
        string password
    )
    {
        uniqueId = EncodeFilter(uniqueId);
        password = EncodeFilter(password);

        var users = await ldapService.SearchAnonymous<PublicUserDetails>(
            searchBase: "ou=users,dc=hush,dc=rip",
            filter: $"(&(uid={uniqueId})(userPassword={password}))"
        );

        return users.Count == 1;
    }

    /// <summary>
    /// Converts an <see cref="LdapChange"/> list into LDIF changes.
    /// </summary>
    /// <param name="dn">
    /// DN that the changes target.
    /// </param>
    /// <param name="changes">
    /// Changes to apply.
    /// </param>
    /// <returns>
    /// LDIF representation of the changes.
    /// </returns>
    public static string GenerateModifyLdif(string dn, List<LdapChange> changes)
    {
        string output = "dn: ";
        output += dn;
        output += "\nchangetype: modify\n";
        output += string.Join("\n-\n", changes.Select(c => c.ToString()));
        return output;
    }

    /// <summary>
    /// Creates a new LDAP user and adds them to the readers group.
    /// </summary>
    /// <param name="ldapService">
    /// LdapUtilsService to execute the command.
    /// </param>
    /// <param name="ou">
    /// Organizational unit of the new user.
    /// </param>
    /// <param name="uniqueId">
    /// Unique ID of the new user.
    /// </param>
    /// <param name="email">
    /// Email address of the new user.
    /// </param>
    /// <param name="password">
    /// Password of the new user.
    /// </param>
    /// <returns>
    /// Whether the operations were successful.
    /// </returns>
    public static async Task<bool> CreateUser(
        LdapUtilsService ldapService,
        string ou,
        string uniqueId,
        string email,
        string password
    )
    {
        var fullUserDN = $"cn={EncodeDN(uniqueId)},ou={ou},dc=hush,dc=rip";
        var ldif =
            $@"dn: {fullUserDN}
cn: {uniqueId}
sn: sn
objectClass: inetOrgPerson
userPassword: {password}
uid: {uniqueId}
mail: {email}";

        // Create user
        var createResult = await ldapService.Add(
            Admin.LdapDN,
            Admin.LdapPassword,
            ldif
        );

        if (createResult.ExitCode != 0)
        {
            return false;
        }

        // Add user to readers
        var modifyResult = await ldapService.Modify(
            Admin.LdapDN,
            Admin.LdapPassword,
            LdapHelper.GenerateModifyLdif(
                "cn=readers,ou=users,dc=hush,dc=rip",
                new()
                {
                    new()
                    {
                        ChangeType = LdapChangeType.Add,
                        Attribute = "member",
                        Value = fullUserDN,
                        ConvertToBase64 = false,
                    },
                }
            )
        );

        return modifyResult.ExitCode == 0;
    }

    /// <summary>
    /// Deletes an LDAP user and removes them from the readers group.
    /// </summary>
    /// <param name="ldapService">
    /// LdapUtilsService to execute the command.
    /// </param>
    /// <param name="uniqueId">
    /// Unique ID of the user to delete.
    /// </param>
    /// <returns>
    /// Whether the operations were successful.
    /// </returns>
    public static async Task<bool> DeleteUser(
        LdapUtilsService ldapService,
        string uniqueId
    )
    {
        var fullUserDN = $"cn={EncodeDN(uniqueId)},ou=users,dc=hush,dc=rip";

        // Delete user
        var deleteResult = await ldapService.Delete(
            Admin.LdapDN,
            Admin.LdapPassword,
            fullUserDN
        );

        if (deleteResult.ExitCode != 0)
        {
            return false;
        }

        // Remove user from readers
        var modifyResult = await ldapService.Modify(
            Admin.LdapDN,
            Admin.LdapPassword,
            LdapHelper.GenerateModifyLdif(
                "cn=readers,ou=users,dc=hush,dc=rip",
                new()
                {
                    new()
                    {
                        ChangeType = LdapChangeType.Delete,
                        Attribute = "member",
                        Value = fullUserDN,
                        ConvertToBase64 = false,
                    },
                }
            )
        );

        return modifyResult.ExitCode == 0;
    }
}
