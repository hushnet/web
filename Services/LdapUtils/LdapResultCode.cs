namespace HushWeb.Services.LdapUtils;

#pragma warning disable

/// <summary>
/// LDAP exit codes, as defined by this ldapwiki page:
/// <see href="https://ldapwiki.com/wiki/LDAP%20Result%20Codes"/>
/// </summary>
public enum LdapResultCode
{
    Success = 0,
    OperationsError = 1,
    ProtocolError = 2,
    TimeLimitExceeded = 3,
    SizeLimitExceeded = 4,
    CompareFalse = 5,
    CompareTrue = 6,
    AuthMethodNotSupported = 7,
    StrongAuthRequired = 8,
    PartialResults = 9,
    Referral = 10,
    AdminLimitExceeded = 11,
    UnavailableCriticalExtension = 12,
    ConfidentialityRequired = 13,
    SaslBindInProgress = 14,
    NoSuchAttribute = 16,
    UndefinedType = 17,
    InappropriateMatching = 18,
    ConstraintViolation = 19,
    TypeOrValueExists = 20,
    InvalidSyntax = 21,
    NoSuchObject = 32,
    AliasProblem = 33,
    InvalidDnSyntax = 34,
    IsLeaf = 35,
    AliasDerefProblem = 36,
    InappropriateAuth = 48,
    InvalidCredentials = 49,
    InsufficientAccess = 50,
    Busy = 51,
    Unavailable = 52,
    UnwillingToPerform = 53,
    LoopDetect = 54,
    NamingViolation = 64,
    ObjectClassViolation = 65,
    NotAllowedOnNonleaf = 66,
    NotAllowedOnRdn = 67,
    AlreadyExists = 68,
    NoObjectClassMods = 69,
    ResultsTooLarge = 70,
    AffectsMultipleDsas = 71,
    Other = 80,
    ServerDown = 81,
    LocalError = 82,
    EncodingError = 83,
    DecodingError = 84,
    Timeout = 85,
    AuthUnknown = 86,
    FilterError = 87,
    UserCancelled = 88,
    ParamError = 89,
    NoMemory = 90,
    ConnectError = 91,
    NotSupported = 92,
    ControlNotFound = 93,
    NoResultsReturned = 94,
    MoreResultsToReturn = 95,
    ClientLoop = 96,
    ReferralLimitExceeded = 97,
    InvalidResponse = 100,
    AmbiguousResponse = 101,
    TlsNotSupported = 112,
    LcupResourcesExhausted = 113,
    LcupSecurityViolation = 114,
    LcupInvalidData = 115,
    LcupUnsupportedScheme = 116,
    LcupReloadRequired = 117,
    Canceled = 118,
    NoSuchOperation = 119,
    TooLate = 120,
    CannotCancel = 121,
    AssertionFailed = 122,
    AuthorizationDenied = 123,
    ESyncRefreshRequired = 4096,
}
