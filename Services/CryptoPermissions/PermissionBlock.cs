namespace HushWeb.Services.CryptoPermissions;

using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

/// <summary>
/// Cryptographic representation of explicitely given permission to perform
/// exactly one administrative action. The rendered permission block must be
/// signed by an administrators key to be considered valid.
/// </summary>
public abstract class PermissionBlock
{
    /// <summary>
    /// Begins the payload of the permission block.
    /// </summary>
    public static readonly string BeginTag = "[BEGIN PERMISSION BLOCK]";

    /// <summary>
    /// Ends the payload of the permission block.
    /// </summary>
    public static readonly string EndTag = "[END PERMISSION BLOCK]";

    /// <summary>
    /// 60 character wide unicode line to use as a horizontal rule.
    /// </summary>
    public static readonly string HorizontalRule = new('━', 60);

    /// <summary>
    /// Initializes a new instance of the <see cref="PermissionBlock"/> class.
    /// </summary>
    /// <param name="permission">
    /// What type of permission the permission block grants.
    /// </param>
    /// <param name="validDuration">
    /// How long the permission is valid.
    /// </param>
    public PermissionBlock(string permission, TimeSpan validDuration)
    {
        Generated = DateTime.UtcNow;
        Expiry = Generated.Add(validDuration);
        Permission = permission;
        Guid = System.Guid.NewGuid().ToString();
    }

    /// <summary>
    /// Renders a header containing the generated and expiry.
    /// </summary>
    [JsonIgnore]
    public string Header =>
        $@"Generated on {Generated.ToShortDateString()} at {Generated.ToShortTimeString()} (UTC)
Expires on {Expiry.ToShortDateString()} at {Expiry.ToShortTimeString()} (UTC)
Only valid when signed by an administrator key.";

    /// <summary>
    /// Renders a permission type tag.
    /// </summary>
    [JsonIgnore]
    public string PermissionTypeTag => $"[PERMISSION TYPE: {Permission}]";

    /// <summary>
    /// Renders a GUID tag.
    /// </summary>
    [JsonIgnore]
    public string GuidTag => $"[GUID: {Guid}]";

    /// <summary>
    /// When the permission block was generated.
    /// </summary>
    [BindNever]
    public DateTime Generated { get; init; }

    /// <summary>
    /// When the permission expires.
    /// </summary>
    [BindNever]
    public DateTime Expiry { get; init; }

    /// <summary>
    /// What type of permission the permission block grants.
    /// </summary>
    [JsonRequired]
    public string Permission { get; init; }

    /// <summary>
    /// Guaranteed unique identifier to prevent reusing the permission block.
    /// </summary>
    [JsonRequired]
    public string Guid { get; init; }

    /// <summary>
    /// Checks <see cref="IsValid"/> and throws an exception if invalid.
    /// </summary>
    public void AssertValid()
    {
        if (!IsValid())
        {
            throw new Exception("Invalid permission block");
        }
    }

    /// <summary>
    /// Encodes the permission block into JSON and encrypts it using the
    /// protector.
    /// </summary>
    /// <param name="protector">
    /// Protector to use for encryption.
    /// </param>
    public string EncodePayload(IDataProtector protector)
    {
        return protector
            .Protect(JsonConvert.SerializeObject(this))
            .Replace('-', '@')
            .WrapByCharacter(60);
    }

    /// <summary>
    /// Formats readable content for use in a permission block message.
    /// </summary>
    public string ReadableContent(string content) =>
        content.Trim().Replace('\n', ' ').WrapByWord(60);

    /// <summary>
    /// Determines if the permission block is completely valid.
    /// </summary>
    public abstract bool IsValid();

    /// <summary>
    /// Renders the permission block.
    /// </summary>
    /// <param name="protector">
    /// Protector to use for encryption.
    /// </param>
    public abstract string ToString(IDataProtector protector);
}
