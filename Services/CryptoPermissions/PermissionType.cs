namespace HushWeb.Services.CryptoPermissions;

/// <summary>
/// Types of permissions to be used in <see cref="PermissionBlock"/>
/// </summary>
public static class PermissionType
{
#pragma warning disable

    public const string CreateAccount = "CreateAccount";
    public const string DeleteAccount = "DeleteAccount";

#pragma warning restore

    /// <summary>
    /// List of all supported permission types.
    /// </summary>
    public static readonly List<string> AllTypes =
        new() { CreateAccount, DeleteAccount };
}
