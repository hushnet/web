namespace HushWeb.Services.CryptoPermissions;

using HushWeb.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.DataProtection;
using Newtonsoft.Json;
using PgpCore;

/// <summary>
/// Represents permission to create an LDAP account.
/// </summary>
public class CreateAccountPermissionBlock : PermissionBlock
{
    private static readonly List<string> ValidateProperties =
        new() { "Email", "UniqueId", "Password" };

    /// <summary>
    /// Initializes a new instance of the <see cref="CreateAccountPermissionBlock"/> class.
    /// </summary>
    public CreateAccountPermissionBlock(ApplicationForm application)
        : base(PermissionType.CreateAccount, TimeSpan.FromDays(30))
    {
        Application = application;
    }

    /// <summary>
    /// User's application.
    /// </summary>
    [JsonRequired]
    public ApplicationForm Application { get; set; }

    /// <summary>
    /// Determines if the permission block is valid. Warning: This assumes the
    /// PGP challenge was already validated!
    /// </summary>
    public override bool IsValid()
    {
        foreach (var propertyName in ValidateProperties)
        {
            var property = typeof(ApplicationForm).GetProperty(propertyName);

            if (property == null)
            {
                throw new Exception("Tried to validate nonexistent property");
            }

            var propertyValue = property.GetValue(Application);
            var context = new ValidationContext(Application)
            {
                MemberName = propertyName,
            };

            var results = new List<ValidationResult>();
            var valid = Validator.TryValidateProperty(
                propertyValue,
                context,
                results
            );

            if (!valid)
            {
                Console.WriteLine(
                    "CreateAccountPermissionBlock validation failed!"
                );
                Console.WriteLine(
                    results.Select(r => $"Error: {r.ErrorMessage}\n")
                );

                return false;
            }
        }

        return true;
    }

    /// <inheritdoc/>
    public override string ToString(IDataProtector protector)
    {
        return $@"
{Header}
{HorizontalRule}


{ReadableContent($@"This message contains a user's pending application form. Generated using the application form on the Apply page.")}

Unique ID: {Application.UniqueId}
Email address: {Application.Email}
Public key fingerprint: {Application!.PublicKeyFingerprint}


{HorizontalRule}
{PermissionTypeTag}
{GuidTag}
{BeginTag}
{EncodePayload(protector)}
{EndTag}";
    }
}
