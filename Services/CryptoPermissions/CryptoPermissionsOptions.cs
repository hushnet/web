namespace HushWeb.Services.CryptoPermissions;

/// <summary>
/// <see cref="CryptoPermissionsService"/> initialization options.
/// </summary>
public struct CryptoPermissionsOptions
{
    /// <summary>
    /// Purpose to specify for the protector use.
    /// </summary>
    public string ProtectorPurpose { get; init; }
}
