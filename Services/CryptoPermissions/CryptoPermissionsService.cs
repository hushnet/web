namespace HushWeb.Services.CryptoPermissions;

using Microsoft.AspNetCore.DataProtection;

/// <summary>
/// Service used for interacting with <see cref="PermissionBlock"/>.
/// </summary>
public class CryptoPermissionsService
{
    private readonly IDataProtector protector;

    /// <summary>
    /// Initializes a new instance of the <see cref="CryptoPermissionsService"/> class.
    /// </summary>
    /// <param name="dataProtectionProvider">
    /// Protector to use for encryption.
    /// </param>
    /// <param name="options">
    /// Initialization options.
    /// </param>
    public CryptoPermissionsService(
        IDataProtectionProvider dataProtectionProvider,
        CryptoPermissionsOptions options
    )
    {
        protector = dataProtectionProvider.CreateProtector(
            options.ProtectorPurpose
        );
    }

    /// <summary>
    /// Renders a <see cref="PermissionBlock"/>.
    /// </summary>
    /// <param name="block">
    /// Permission block to render.
    /// </param>
    /// <exception>
    /// Throws if permission block is invalid.
    /// </exception>
    public string RenderBlock(PermissionBlock block)
    {
        block.AssertValid();
        return block.ToString(protector);
    }

    /// <summary>
    /// Extracts and decrypts the information in a permission block message.
    /// </summary>
    /// <param name="permissionBlockMessage">
    /// Message contained a permission block to extract and decrypt.
    /// </param>
    /// <returns>
    /// JSON representation of a <see cref="PermissionBlock"/>.
    /// </returns>
    public string DecryptPermissionBlock(string permissionBlockMessage)
    {
        var lines = permissionBlockMessage.Split('\n');
        var readingBlock = false;
        var encoded = string.Empty;

        foreach (var line in lines)
        {
            var trimmed = line.Trim();

            if (!readingBlock)
            {
                if (trimmed == PermissionBlock.BeginTag)
                {
                    readingBlock = true;
                }

                continue;
            }

            if (trimmed == PermissionBlock.EndTag)
            {
                break;
            }

            encoded += trimmed;
        }

        return protector.Unprotect(encoded.Replace('@', '-'));
    }
}
