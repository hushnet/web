namespace HushWeb.Services.CryptoPermissions;

using Microsoft.AspNetCore.DataProtection;
using Newtonsoft.Json;

/// <summary>
/// Represents permission to delete an LDAP account.
/// </summary>
public class DeleteAccountPermissionBlock : PermissionBlock
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DeleteAccountPermissionBlock"/> class.
    /// </summary>
    public DeleteAccountPermissionBlock()
        : base(PermissionType.DeleteAccount, TimeSpan.FromDays(1)) { }

    /// <summary>
    /// Unique ID of the user to delete.
    /// </summary>
    [JsonRequired]
    public string? UniqueId { get; init; }

    /// <inheritdoc/>
    public override bool IsValid() => !string.IsNullOrWhiteSpace(UniqueId);

    /// <inheritdoc/>
    public override string ToString(IDataProtector protector)
    {
        return $@"
{Header}
{HorizontalRule}


{ReadableContent($@"This message grants the bearer permission to delete the user account with the specified unique ID.")}

Unique ID: {UniqueId}


{HorizontalRule}
{PermissionTypeTag}
{GuidTag}
{BeginTag}
{EncodePayload(protector)}
{EndTag}";
    }
}
