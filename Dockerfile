# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk AS build-env
WORKDIR /app
COPY *.csproj ./
RUN dotnet restore
COPY . ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:6.0
# Remove the following line to leave debug mode
ENV ASPNETCORE_ENVIRONMENT=Development
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y ldap-utils python3
COPY ./python /app/python
ENV PATH=$PATH:/app/python
WORKDIR /app
RUN mkdir keys
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "HushWeb.dll"]
EXPOSE 5000